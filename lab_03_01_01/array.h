#ifndef ARRAY_H
#define ARRAY_H

#include <stdio.h>

#define N_MAX 10
#define OK 0
#define ERR 1
#define ERR_OUT_RANGE 2
#define ERR_NO_ELEMENT 3
#define ERR_OVERFLOW 4
#define ERR_IDX_OUT 5

int input_number_of_matrix(size_t *row, size_t *col);
int input_array(int a[N_MAX][N_MAX], size_t row, size_t col);
void print_array_b(const int *a, size_t n);
void check_symmetric(int a[N_MAX][N_MAX], size_t row, size_t col, int *b);

#endif /* ARRAY_H */
