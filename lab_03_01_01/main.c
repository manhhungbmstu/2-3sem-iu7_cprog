#include <stdio.h>
#include "array.h"

int main(void)
{
    size_t row = 0;
    size_t col = 0;
    int a[N_MAX][N_MAX];
    int b[N_MAX];

    if (input_number_of_matrix(&row, &col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }

    if (input_array(a, row, col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }

    check_symmetric(a, row, col, b);

    print_array_b(b, row);

    return OK;
}
