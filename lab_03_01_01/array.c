#include "array.h"

int input_number_of_matrix(size_t *row, size_t *col)
{
    printf("Введите номер строки: ");
    if (scanf("%zu", row) != 1)
    {
        printf("Error: Ввод/Вывод\n");
        return ERR;
    }
    if (*row <= 0 || *row > N_MAX)
    {
        printf("Ошибка: значение строка вне допустимого диапазона\n");
        return ERR_OUT_RANGE;
    }

    printf("Введите номер столбики: ");
    if (scanf("%zu", col) != 1)
    {
        printf("Error: Ввод/Вывод\n");
        return ERR;
    }
    if (*col <= 0 || *col > N_MAX)
    {
        printf("Ошибка: значение столбца вне допустимого диапазона\n");
        return ERR_OUT_RANGE;
    }

    return OK;
}

int input_array(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    printf("Input array element:\n");
    for (size_t i = 0; i < row; i++)
        for (size_t j = 0; j < col; j++)
        {
            printf("a[%ld][%ld]: ", i, j);
            if (scanf("%d", &a[i][j]) != 1)
            {
                printf("Error: Ввод/Вывод\n");
                return ERR;
            }
        }

    return OK;
}

void print_array_b(const int a[], size_t n)
{
    printf("Array element:\n");
    for (size_t i = 0; i < n; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}

void check_symmetric(int a[N_MAX][N_MAX], size_t row, size_t col, int *b)
{
    for (size_t i = 0; i < row; i++)
    {
        int is_symmetric = 1;
        for (size_t j = 0; j < col / 2; j++)
        {
            if (a[i][j] != a[i][col - j - 1])
            {
                is_symmetric = 0;
                break;
            }
        }
        b[i] = is_symmetric;
    }
}
