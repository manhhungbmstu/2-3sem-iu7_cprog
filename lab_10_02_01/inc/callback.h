#ifndef CALLBACK_H__
#define CALLBACK_H__

#include <stddef.h>
#include <stdio.h>

/**
 * @brief Тип функция сравнения
 */
typedef int (*compare_func_t)(const void *pl, const void *pr);
/**
 * @brief Тип функция освобождение памяти
 */
typedef void (*free_func_t)(void *data);


#endif
