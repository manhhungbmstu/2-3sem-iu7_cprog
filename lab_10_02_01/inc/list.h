#ifndef LIST_H__
#define LIST_H__

#include "callback.h"

// Структура списка
typedef struct node node_t;
struct node
{
    void *data;   // Значение
    node_t *next; // Следующий элемент
};

/**
 * @brief Вставляет элемент перед указанным элементом списка
 */
void insert(node_t **head, node_t *elem, node_t *before);

/**
 * @brief Создание нового элемента
 */
node_t *list_new(void);

/**
 * @brief Освобождение памяти элемент списка, без освобождение `data`
 */
void list_free(node_t *head);

/**
 * @brief Освобождение памяти элемент списка, с освобождение `data`
 */
void list_free_full(node_t *head, free_func_t f);
/**
 * @brief Два списка равны?
 */
int list_is_equal(const node_t *list_a, const node_t *list_b, compare_func_t cmp);

#endif
