#ifndef POLYNOMIAL_H__
#define POLYNOMIAL_H__

#include "list.h"
#define COMMAND_VAL "val"
#define COMMAND_DDX "ddx"
#define COMMAND_SUM "sum"
#define COMMAND_DVD "dvd"
// Структура для представления одного члена полинома
typedef struct
{
    int coeff;  // Коэффициент
    int degree; // Степень
} term_t;
typedef enum
{
    COMMAND_UNKNOWN,
    COMMAND_VAL_TYPE,
    COMMAND_DDX_TYPE,
    COMMAND_SUM_TYPE,
    COMMAND_DVD_TYPE
} commandtype;
void print_poly(const node_t *head);

int read_poly(node_t **poly);

/**
 * @brief Функция для вычисления значения полинома P(a)
 */
int evaluate_poly(const node_t *head, int a);

/**
 * @brief Функция для вычисления производной полинома d/dx * P(x)
 */
node_t *derivative_poly(node_t *head);

/**
 * @brief Функция для сложения двух полиномов
 */
node_t *sum_poly(node_t *p1, node_t *p2);
node_t *create_poly_node(int coeff, int degree);
/**
 * @brief Функция для разделения полинома на чётные и нечётные степени
 */
int divide_poly(node_t *head, node_t **even, node_t **odd);

#endif
