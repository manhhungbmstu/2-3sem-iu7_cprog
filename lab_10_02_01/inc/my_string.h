#ifndef MY_STRING_H__
#define MY_STRING_H__

#include <stdio.h>

/**
 * @brief Читать строку из файла
 */
int str_read(char **data, FILE *f);

#endif
