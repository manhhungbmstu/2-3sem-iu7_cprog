#ifndef ERROR_H__
#define ERROR_H__

#define OK 0

#define ERR_FILE 1
#define ERR_STR_IO 2
#define ERR_STR_RANGE 3
#define ERR_MEM 4

#define ERR_IO 5

#define ERR_POLY 6

#define ERR_USAGE 53
#define ERR_INVALID_ARGS -3
#define ERR_MEMORY -4
#define ERR_EMPTY_FILE -5
#define INIT_CAPACITY 64
void err_print(const int err_code);

#endif
