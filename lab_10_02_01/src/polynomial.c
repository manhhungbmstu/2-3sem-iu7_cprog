#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "polynomial.h"
#include "my_error.h"
#include "my_string.h"

void print_poly(const node_t *head)
{
    if (head)
    {
        while (head)
        {
            term_t *term = (term_t *)head->data;
            printf("%d %d ", term->coeff, term->degree);
            head = head->next;
        }
        printf("L\n");
    }
}

static bool check_poly(node_t *head)
{
    while (head && head->next)
    {
        term_t *current = (term_t *)head->data;
        term_t *next = (term_t *)head->next->data;

        if (current->degree <= next->degree)
        {
            return false;
        }

        head = head->next;
    }

    return true;
}

int read_poly(node_t **head)
{
    int coeff, degree;
    int rc = OK;

    char *str;
    rc = str_read(&str, stdin);
    if (rc)
        return rc;
    char *head_str = str;

    while (str && (rc = sscanf(str, "%d %d", &coeff, &degree)) == 2)
    {
        term_t *term = malloc(sizeof(term_t));
        if (!term)
        {
            list_free_full(*head, free);
            free(head_str);
            return ERR_MEM;
        }
        term->degree = degree;
        term->coeff = coeff;

        node_t *node = list_new();
        if (!node)
        {
            list_free_full(*head, free);
            free(head_str);
            return ERR_MEM;
        }
        node->data = term;

        insert(head, node, NULL);

        str = strchr(str, ' ');
        if (str)
        {
            str++;
            str = strchr(str, ' ');
            if (str)
                str++;
        }
    }

    if (rc != 2 || !check_poly(*head))
    {
        list_free_full(*head, free);
        free(head_str);
        *head = NULL;
        return (rc == 2) ? ERR_POLY : ERR_IO;
    }

    free(head_str);
    return OK;
}

static int int_pow(int base, int power)
{
    if (power == 0)
    {
        return 1;
    }
    else if (power % 2 == 0)
    {
        int half_pow = int_pow(base, power / 2);
        return half_pow * half_pow;
    }
    else
    {
        return base * int_pow(base, power - 1);
    }
}

static int evaluate_term(const term_t *term, int a)
{
    return term->coeff * int_pow(a, term->degree);
}

int evaluate_poly(const node_t *head, int a)
{
    int sum = 0;

    while (head)
    {
        term_t *term = (term_t *)head->data;
        sum += evaluate_term(term, a);
        head = head->next;
    }

    return sum;
}

node_t *derivative_poly(node_t *head)
{
    node_t *result_head = NULL;
    node_t *result_tail = NULL;

    while (head)
    {
        term_t *term = (term_t *)head->data;
        int new_coeff = term->coeff * term->degree;

        if (new_coeff != 0)
        {
            term_t *new_term = malloc(sizeof(term_t));
            new_term->coeff = new_coeff;
            new_term->degree = term->degree - 1;

            node_t *new_node = malloc(sizeof(node_t));
            new_node->data = new_term;
            new_node->next = NULL;

            if (result_tail)
            {
                result_tail->next = new_node;
                result_tail = new_node;
            }
            else
            {
                result_head = result_tail = new_node;
            }
        }

        head = head->next;
    }

    return result_head;
}

node_t *create_poly_node(int coeff, int degree)
{
    term_t *term = malloc(sizeof(term_t));
    if (!term)
    {
        return NULL;
    }

    term->coeff = coeff;
    term->degree = degree;

    node_t *node = list_new();
    if (!node)
    {
        free(term);
        return NULL;
    }

    node->data = term;
    return node;
}

node_t *sum_poly(node_t *p1, node_t *p2)
{
    node_t *shead = NULL;
    node_t *current_result = NULL;

    while (p1 || p2)
    {
        term_t *t1 = (p1) ? (term_t *)p1->data : NULL;
        term_t *t2 = (p2) ? (term_t *)p2->data : NULL;

        term_t *sterm = calloc(1, sizeof(term_t));
        if (!sterm)
        {
            list_free_full(shead, free);
            return NULL;
        }

        if (t1 && t2)
        {
            if (t1->degree == t2->degree)
            {
                sterm->coeff += t1->coeff + t2->coeff;
                sterm->degree = t1->degree;
                t1 = t2 = NULL;
                p1 = p1->next;
                p2 = p2->next;
            }
            else if (t1->degree > t2->degree)
            {
                sterm->coeff += t1->coeff;
                sterm->degree = t1->degree;
                t1 = NULL;
                p1 = p1->next;
            }
            else
            {
                sterm->coeff += t2->coeff;
                sterm->degree = t2->degree;
                t2 = NULL;
                p2 = p2->next;
            }
        }
        else if (t1)
        {
            sterm->coeff += t1->coeff;
            sterm->degree = t1->degree;
            t1 = NULL;
            p1 = p1->next;
        }
        else if (t2)
        {
            sterm->coeff += t2->coeff;
            sterm->degree = t2->degree;
            t2 = NULL;
            p2 = p2->next;
        }

        node_t *node = create_poly_node(sterm->coeff, sterm->degree);
        if (!node)
        {
            free(sterm);
            list_free_full(shead, free);
            return NULL;
        }

        if (!shead)
        {
            shead = node;
            current_result = shead;
        }
        else
        {
            current_result->next = node;
            current_result = node;
        }

        free(sterm);
    }

    return shead;
}
void free_list(node_t *head)
{
    while (head)
    {
        node_t *temp = head;
        head = head->next;

        term_t *term = (term_t *)temp->data;
        free(term);
        free(temp);
    }
}

int divide_poly(node_t *head, node_t **even, node_t **odd)
{
    if (!head || !even || !odd)
        return -1;

    *even = NULL;
    *odd = NULL;

    node_t *current_even = NULL;
    node_t *current_odd = NULL;
    int count = 0;

    while (head)
    {
        term_t *term = (term_t *)head->data;

        if (term)
        {
            node_t *new_node = malloc(sizeof(node_t));
            if (!new_node)
            {
                free_list(*even);
                free_list(*odd);
                return ERR_MEM;
            }
            new_node->data = term;
            new_node->next = NULL;

            if (term->degree % 2 == 0)
            {
                if (!*even)
                {
                    *even = new_node;
                    current_even = new_node;
                }
                else
                {
                    insert(&current_even, new_node, NULL);
                    current_even = new_node;
                }
            }
            else
            {
                if (!*odd)
                {
                    *odd = new_node;
                    current_odd = new_node;
                }
                else
                {
                    insert(&current_odd, new_node, NULL);
                    current_odd = new_node;
                }
            }

            count++;
        }

        head = head->next;
    }

    return count;
}
