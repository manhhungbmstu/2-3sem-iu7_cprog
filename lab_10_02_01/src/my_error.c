#include <stdio.h>
#include <string.h>

#include "my_error.h"

void err_print(const int err_code)
{
    switch (err_code)
    {
        case ERR_USAGE:
            fprintf(stdout, "app.exe IN_FILE\n");
            break;
        case ERR_STR_IO:
            fprintf(stderr, "Error: Ввод/Вывод строка\n");
            break;
        case OK:
            break;
        default:
            fprintf(stderr, "Error: %d (not named yet)!\n", err_code);
            break;
    }
}
