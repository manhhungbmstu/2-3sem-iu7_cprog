#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "my_error.h"
#include "my_string.h"

int str_read(char **data, FILE *f)
{
    if (!data || !f)
    {
        return ERR_INVALID_ARGS;
    }

    size_t capacity = INIT_CAPACITY;
    size_t size = 0;
    int c;

    *data = (char *)malloc(capacity * sizeof(char));
    if (!(*data))
    {
        return ERR_MEMORY;
    }

    while ((c = getc(f)) != EOF && c != '\n')
    {
        if (size + 1 >= capacity)
        {
            capacity *= 2;
            char *temp = (char *)realloc(*data, capacity * sizeof(char));
            if (!temp)
            {
                free(*data);
                return ERR_MEMORY;
            }
            *data = temp;
        }

        (*data)[size++] = (char)c;
    }

    if (size == 0 && c == EOF)
    {
        // файл пуст
        free(*data);
        return ERR_EMPTY_FILE;
    }

    (*data)[size] = '\0';

    return OK;
}
