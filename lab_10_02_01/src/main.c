#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "my_error.h"
#include "polynomial.h"
#include "list.h"
#include "my_string.h"

commandtype get_command_type(const char *command)
{
    if (strcmp(command, COMMAND_VAL) == 0)
    {
        return COMMAND_VAL_TYPE;
    }
    else if (strcmp(command, COMMAND_DDX) == 0)
    {
        return COMMAND_DDX_TYPE;
    }
    else if (strcmp(command, COMMAND_SUM) == 0)
    {
        return COMMAND_SUM_TYPE;
    }
    else if (strcmp(command, COMMAND_DVD) == 0)
    {
        return COMMAND_DVD_TYPE;
    }
    else
    {
        return COMMAND_UNKNOWN;
    }
}
int main()
{
    char *command = NULL;
    int rc = str_read(&command, stdin);

    if (rc != OK)
    {
        free(command);
        return rc;
    }

    node_t *poly = NULL;
    commandtype cmd_type = get_command_type(command);

    if (cmd_type == COMMAND_VAL_TYPE)
    {
        rc = read_poly(&poly);
        if (rc == OK)
        {
            int a;
            if (scanf("%d", &a) == 1)
            {
                int result = evaluate_poly(poly, a);
                printf("%d\n", result);
            }
        }
    }
    else if (cmd_type == COMMAND_DDX_TYPE)
    {
        rc = read_poly(&poly);
        if (rc == OK)
        {
            node_t *ddx = derivative_poly(poly);
            print_poly(ddx);
            list_free_full(ddx, free);
        }
    }
    else if (cmd_type == COMMAND_SUM_TYPE)
    {
        node_t *poly1 = NULL, *poly2 = NULL;
        rc = read_poly(&poly1);
        if (rc == OK)
        {
            rc = read_poly(&poly2);
            if (rc == OK)
            {
                node_t *sum = sum_poly(poly1, poly2);
                print_poly(sum);
                list_free_full(sum, free);
                list_free_full(poly2, free);
            }
            list_free_full(poly1, free);
        }
    }
    else if (cmd_type == COMMAND_DVD_TYPE)
    {
        node_t *poly = NULL;
        rc = read_poly(&poly);
        if (rc == OK)
        {
            node_t *even = NULL, *odd = NULL;
            divide_poly(poly, &even, &odd);
            print_poly(even);
            print_poly(odd);

            list_free(even);
            list_free(odd);
            list_free_full(poly, free);
        }
    }
    else
    {
        rc = ERR_USAGE;
    }

    free(command);
    list_free_full(poly, free);

    return rc;
}
