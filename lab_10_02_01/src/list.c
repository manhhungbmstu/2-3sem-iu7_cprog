#include <stdlib.h>
#include <assert.h>

#include "list.h"

void insert(node_t **head, node_t *elem, node_t *before)
{
    if (!elem || !head)
    {
        return;
    }

    if (!before)
    {
        if (!*head)
        {
            elem->next = NULL;
            *head = elem;
        }
        else
        {
            node_t *curr = *head;
            while (curr->next)
            {
                curr = curr->next;
            }
            curr->next = elem;
            elem->next = NULL;
        }
    }
    else if (before == *head)
    {
        elem->next = *head;
        *head = elem;
    }
    else
    {
        node_t *curr = *head;
        while (curr && curr->next != before)
        {
            curr = curr->next;
        }
        if (curr)
        {
            elem->next = before;
            curr->next = elem;
        }
    }
}

// endregion

// region MY

node_t *list_new(void)
{
    node_t *new = malloc(sizeof(node_t));
    if (new)
    {
        new->data = NULL;
        new->next = NULL;
    }
    return new;
}

void list_free(node_t *head)
{
    while (head)
    {
        node_t *tmp = head;
        head = head->next;
        free(tmp);
    }
}

void list_free_full(node_t *head, free_func_t f)
{
    while (head)
    {
        node_t *tmp = head;
        head = head->next;
        f(tmp->data);
        free(tmp);
    }
}

int list_is_equal(const node_t *list_a, const node_t *list_b, compare_func_t cmp)
{
    while (list_a != NULL && list_b != NULL)
    {
        if (cmp(list_a->data, list_b->data) != 0)
        {
            // Если элементы не равны, списки не равны
            return 0;
        }
        list_a = list_a->next;
        list_b = list_b->next;
    }
    // Если один из списков не пуст, они не равны
    if (list_a != NULL || list_b != NULL)
    {
        return 0;
    }
    // Списки равны
    return 1;
}

// endregion
