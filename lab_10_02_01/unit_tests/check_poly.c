#include <stdio.h>
#include <stdlib.h>

#include "check_poly.h"
#include "polynomial.h"
#include "list.h"
#include "my_error.h"

term_t *create_test_term(int coeff, int degree)
{
    term_t *term = malloc(sizeof(term_t));
    if (term == NULL)
    {

        return NULL;
    }

    term->coeff = coeff;
    term->degree = degree;
    return term;
}

void free_test_term(term_t *term)
{
    free(term);
}

node_t *create_test_node(term_t *term)
{
    if (term == NULL)
    {

        return NULL;
    }

    node_t *node = malloc(sizeof(node_t));
    if (node == NULL)
    {

        return NULL;
    }

    node->data = term;
    node->next = NULL;
    return node;
}

void free_test_node(node_t *node)
{
    if (node != NULL)
    {
        free_test_term((term_t *)node->data);
        free(node);
    }
}


START_TEST(test_evaluate_poly)
{
    int a = 2;

    //  3*a^2 + 2*a^1 + 1*a^0
    node_t *term3 = create_test_node(create_test_term(3, 2));
    node_t *term2 = create_test_node(create_test_term(2, 1));
    node_t *term1 = create_test_node(create_test_term(1, 0));

    term3->next = term2;
    term2->next = term1;

    int result = evaluate_poly(term3, a);

    ck_assert_int_eq(result, 17);

    free_test_node(term1);
    free_test_node(term2);
    free_test_node(term3);
}
END_TEST

START_TEST(test_evaluate_poly_as_exemple)
{
    int a = 7;

    // 4*a^2 + 2*a^1
    node_t *term3 = create_test_node(create_test_term(4, 2));
    node_t *term2 = create_test_node(create_test_term(1, 0));

    term3->next = term2;
    // term2->next = term1;

    int result = evaluate_poly(term3, a);

    ck_assert_int_eq(result, 197);

    // free_test_node(term1);
    free_test_node(term2);
    free_test_node(term3);
}
END_TEST
static int compare_polynomials(node_t *poly1, node_t *poly2)
{
    while (poly1 && poly2)
    {
        term_t *term1 = (term_t *)poly1->data;
        term_t *term2 = (term_t *)poly2->data;

        if (term1->coeff != term2->coeff || term1->degree != term2->degree)
        {
            return 0;
        }

        poly1 = poly1->next;
        poly2 = poly2->next;
    }

    return poly1 == NULL && poly2 == NULL;
}

START_TEST(test_derivative_single_term)
{
    // Создаем тестовый полином: 2*x^3
    node_t *term1 = create_test_node(create_test_term(2, 3));

    // Ожидаемая производная: 6*x^2
    node_t *expected_derivative = create_test_node(create_test_term(6, 2));

    node_t *result_derivative = derivative_poly(term1);

    ck_assert_int_eq(compare_polynomials(result_derivative, expected_derivative), 1);

    free_test_node(result_derivative);
    free_test_node(expected_derivative);

    free_test_node(term1);
}
END_TEST

START_TEST(test_sum_poly_simple)
{
    //  3*x^1
    node_t *poly1_term1 = create_test_node(create_test_term(3, 1));

    //  1*x^1
    node_t *poly2_term1 = create_test_node(create_test_term(1, 1));

    // sum: 4*x^1
    node_t *expected_sum_term2 = create_test_node(create_test_term(4, 1));

    node_t *result_sum = sum_poly(poly1_term1, poly2_term1);

    ck_assert_int_eq(compare_polynomials(result_sum, expected_sum_term2), 1);

    free_test_node(poly1_term1);
    free_test_node(poly2_term1);
    free_test_node(expected_sum_term2);
    free_test_node(result_sum);
}
END_TEST

// START_TEST(test_divide_simple_poly)
// {
//     // 2*x^2
//     node_t *poly_term2 = create_test_node(create_test_term(2, 2));

//     node_t *expected_even_term2 = create_test_node(create_test_term(2, 2));

//     expected_even_term2->next = NULL;

//     node_t *expected_odd_term0 = create_test_node(create_test_term(0, 0));

//     expected_odd_term0->next = NULL;

//     node_t *result_even = NULL;
//     node_t *result_odd = NULL;

//     int count = divide_poly(poly_term2, &result_even, &result_odd);

//     ck_assert_int_eq(count, 1);
//     ck_assert_int_eq(compare_polynomials(result_even, expected_even_term2), 1);

//     ck_assert_ptr_eq(result_odd, NULL);

//     free_test_node(poly_term2);
//     free_test_node(expected_even_term2);
//     free_test_node(expected_odd_term0);
//     free_test_node(result_even);
//     free_test_node(result_odd);
// }
// END_TEST

Suite *poly_suite(void)
{
    Suite *suite = suite_create("poly");
    TCase *tc_pos = tcase_create("pos");

    tcase_add_test(tc_pos, test_evaluate_poly);
    tcase_add_test(tc_pos, test_evaluate_poly_as_exemple);

    tcase_add_test(tc_pos, test_derivative_single_term);

    tcase_add_test(tc_pos, test_sum_poly_simple);

    // tcase_add_test(tc_pos, test_divide_simple_poly);

    suite_add_tcase(suite, tc_pos);
    return suite;
}
