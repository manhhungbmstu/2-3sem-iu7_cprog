#include <assert.h>
#include <math.h>

#include "file.h"

/**
 * @brief Искать среднее значение
 */
int find_mean(FILE *f, double *mean)
{
    assert(f != NULL);
    assert(mean != NULL);

    double sum = 0;
    int n = 0;

    while (fscanf(f, "%lf", mean) == 1)
    {
        n++;
        sum += *mean;
    }

    if (n > 0)
    {
        *mean = sum / n;
        return OK;
    }
    else if (feof(f))
    {
        return ERR_READ;
    }
    else
    {
        return ERR_READ;
    }
}

/**
 * @brief Искать число, наиболее близкое по значению к среднему значению всех чисел
 */
double find_nearest_to_mean(FILE *f, double mean)
{
    assert(f != NULL);

    double num, nearest;
    double dist1, dist2;
    bool isFirstNumber = true;

    while (fscanf(f, "%lf", &num) == 1)
    {
        if (isFirstNumber)
        {
            nearest = num;
            dist1 = fabs(mean - nearest);
            isFirstNumber = false;
        }
        else
        {
            dist2 = fabs(mean - num);
            if (dist2 < dist1)
            {
                nearest = num;
                dist1 = dist2;
            }
        }
    }

    return nearest;
}

int process(FILE *f, double *num)
{
    assert(f != NULL);
    assert(num != NULL);

    int rc = OK;
    double mean;

    rc = find_mean(f, &mean);
    if (rc == OK)
    {
        if (fseek(f, 0, SEEK_SET) == 0)
        {
            *num = find_nearest_to_mean(f, mean);
        }
        else
        {
            rc = ERR_READ;
        }
    }

    return rc;
}
