#ifndef _FILE_H_
#define _FILE_H_

#include <errno.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

#define EPS 0.0000001 /** для сравнения ЧПТ */
#define OK 0           /** Все хорошо */
#define ERR_READ -1    /** ошибка: Чтение файла */
#define ERR_USAGE -100 /** ошибка: Использование */

void swap_dbl(double *dbl1, double *dbl2);
int process(FILE *f, double *num);
void swap_dbl(double *dbl1, double *dbl2);

#endif // _FILE_H_
