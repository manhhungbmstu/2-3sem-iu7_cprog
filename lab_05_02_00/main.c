#include <stdio.h>
#include "file.h"

void print_error(int err)
{
    switch (err)
    {
        case ERR_READ:
            printf("Error: Чтение файла\n");
            break;
        case OK:
            break;
        default:
            printf("Error: Неизвестный код!\n");
            break;
    }
}
void usage(void)
{
    printf("app.exe FILENAME\n");
}

int main(int argc, char **argv)
{
    int rc = OK;
    FILE *f;
    double num;

    if (argc != 2)
    {
        usage();
        return ERR_USAGE;
    }

    f = fopen(argv[1], "r");
    if (f == NULL)
    {
        rc = errno;
        print_error(rc);
        return rc;
    }

    rc = process(f, &num);
    fclose(f);

    if (rc)
    {
        print_error(rc);
        return rc;
    }
    printf("%lf\n", num);

    return rc;
}