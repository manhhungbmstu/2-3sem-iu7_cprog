#include "process.h"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("app.exe: <имя файла>\n");
        return ERR_ARG;
    }

    const char *filename = argv[1];

    printf("Добавьте студента:\n");

    Student student;

    printf("Фамилия: ");
    scanf("%s", student.last_name);

    printf("Рост (в см): ");
    scanf("%u", &student.height); 

    add_student(student, filename);
    printf("Все студенты:\n");
    print_students(filename);


    remove_students_below_average(filename);

    printf("Студенты после удаления:\n");
    print_students(filename);

    return 0;
}
