#ifndef _PROCESS_H_
#define _PROCESS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#define MAX_LAST_NAME_LENGTH (5 + 1)
#define OK 0
#define ERR_IO -1
#define ERR_ARG -2
#define ERR_FILE -3
typedef struct
{
    char last_name[MAX_LAST_NAME_LENGTH];
    uint32_t height;
} Student;

int add_student(Student student, const char *filename);
int print_students(const char *filename);
int remove_students_below_average(const char *filename);


#endif
