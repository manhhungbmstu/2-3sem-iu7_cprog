#include "process.h"

int add_student(Student student, const char *filename)
{
    FILE *file = fopen(filename, "a");
    if (file == NULL)
    {
        printf("Ошибка при открытии файла.\n");
        return ERR_FILE;
    }

    fprintf(file, "%s %u\n", student.last_name, student.height); 
    fclose(file);
    printf("Студент успешно добавлен.\n");
    return 0;
}

int print_students(const char *filename)
{
    FILE *file = fopen(filename, "r");
    if (file == NULL)
    {
        printf("Ошибка при открытии файла.\n");
        return ERR_FILE;
    }

    char last_name[MAX_LAST_NAME_LENGTH];
    uint32_t height;

    while (fscanf(file, "%s %u\n", last_name, &height) != EOF) 
    {
        printf("Фамилия: %s, Рост: %u см\n", last_name, height); 
    }

    fclose(file);
    return 0;
}

int remove_students_below_average(const char *filename)
{
    FILE *file = fopen(filename, "r");
    if (file == NULL)
    {
        printf("Ошибка при открытии файла.\n");
        return ERR_FILE;
    }

    Student students[100];
    int count = 0;
    uint32_t total_height = 0;

    char last_name[MAX_LAST_NAME_LENGTH];
    uint32_t height;

    while (fscanf(file, "%s %u\n", last_name, &height) != EOF) 
    {
        strncpy(students[count].last_name, last_name, MAX_LAST_NAME_LENGTH - 1);
        students[count].last_name[MAX_LAST_NAME_LENGTH - 1] = '\0';
        students[count].height = height;
        total_height += height;
        count++;
    }

    if (count == 0)
    {
        printf("Файл пуст. Нет студентов для удаления.\n");
        fclose(file);
        return ERR_IO;
    }

    uint32_t average_height = total_height / count;

    fclose(file);

    file = fopen(filename, "w");
    if (file == NULL)
    {
        printf("Ошибка при открытии файла.\n");
        return ERR_FILE;
    }

    for (int i = 0; i < count; i++)
    {
        if (students[i].height >= average_height)
        {
            fprintf(file, "%s %u\n", students[i].last_name, students[i].height); 
        }
    }

    fclose(file);
    printf("Студенты с ростом ниже среднего удалены.\n");
    return 0;
}
