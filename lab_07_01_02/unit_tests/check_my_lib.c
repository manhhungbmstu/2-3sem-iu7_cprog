#include <stdio.h>
#include <stdlib.h>

#include "check_my_lib.h"
#include "my_error.h"
#include "my_lib.h"
#include "my_function.h"
#include "array.h"
#define FILE_TMP "/tmp/check_my_lib.c"

// region key

// Нет отрицательный элемент
START_TEST(test_key_no_negative_element)
{
    const int input[] = {1, 2, 3, 4, 5, 6};
    int *dest_begin = NULL;
    int *dest_end = NULL;

    int result = key(input, input + sizeof(input) / sizeof(input[0]), &dest_begin, &dest_end);

    ck_assert_int_eq(result, OK);
    ck_assert_ptr_ne(dest_begin, NULL);
    ck_assert_ptr_ne(dest_end, NULL);
    ck_assert_ptr_ne(dest_begin, dest_end);

    int expected_output[] = {1, 2, 3, 4, 5, 6};
    for (size_t i = 0; i < sizeof(expected_output) / sizeof(expected_output[0]); ++i)
    {
        ck_assert_int_eq(dest_begin[i], expected_output[i]);
    }

    free(dest_begin);
}
END_TEST

// Последний элемент - отрицательный
START_TEST(test_key_last_element_negative)
{
    const int input[] = {1, 2, 3, 4, 5, -6};
    int *dest_begin = NULL;
    int *dest_end = NULL;

    int result = key(input, input + sizeof(input) / sizeof(input[0]), &dest_begin, &dest_end);

    ck_assert_int_eq(result, OK);
    ck_assert_ptr_ne(dest_begin, NULL);
    ck_assert_ptr_ne(dest_end, NULL);
    ck_assert_ptr_ne(dest_begin, dest_end);

    int expected_output[] = {1, 2, 3, 4, 5};
    for (size_t i = 0; i < sizeof(expected_output) / sizeof(expected_output[0]); ++i)
    {
        ck_assert_int_eq(dest_begin[i], expected_output[i]);
    }

    free(dest_begin);
}
END_TEST

// Отрицательный элемент не 1-ый и не последний
START_TEST(test_key_middle_element_negative)
{
    const int input[] = {1, 2, 3, -4, 5, 6};
    int *dest_begin = NULL;
    int *dest_end = NULL;

    int result = key(input, input + sizeof(input) / sizeof(input[0]), &dest_begin, &dest_end);

    ck_assert_int_eq(result, OK);
    ck_assert_ptr_ne(dest_begin, NULL);
    ck_assert_ptr_ne(dest_end, NULL);
    ck_assert_ptr_ne(dest_begin, dest_end);

    int expected_output[] = {1, 2, 3};
    for (size_t i = 0; i < sizeof(expected_output) / sizeof(expected_output[0]); ++i)
    {
        ck_assert_int_eq(dest_begin[i], expected_output[i]);
    }

    free(dest_begin);
}
END_TEST

// Несколько отрицательный элемент
START_TEST(test_key_multiple_negative_elements)
{
    const int input[] = {1, 2, 3, -4, -5, 6};
    int *dest_begin = NULL;
    int *dest_end = NULL;

    int result = key(input, input + sizeof(input) / sizeof(input[0]), &dest_begin, &dest_end);

    ck_assert_int_eq(result, OK);
    ck_assert_ptr_ne(dest_begin, NULL);
    ck_assert_ptr_ne(dest_end, NULL);
    ck_assert_ptr_ne(dest_begin, dest_end);

    int expected_output[] = {1, 2, 3};
    for (size_t i = 0; i < sizeof(expected_output) / sizeof(expected_output[0]); ++i)
    {
        ck_assert_int_eq(dest_begin[i], expected_output[i]);
    }

    free(dest_begin);
}
END_TEST
// проверяет что 2 элементы отрицательные
START_TEST(test_key_two_negative_elements)
{
    const int input[] = {1, 2, 3, -4, 5, -6};
    int *dest_begin = NULL;
    int *dest_end = NULL;

    int result = key(input, input + sizeof(input) / sizeof(input[0]), &dest_begin, &dest_end);

    ck_assert_int_eq(result, OK);
    ck_assert_ptr_ne(dest_begin, NULL);
    ck_assert_ptr_ne(dest_end, NULL);
    ck_assert_ptr_ne(dest_begin, dest_end);

    int expected_output[] = {1, 2, 3};
    for (size_t i = 0; i < sizeof(expected_output) / sizeof(expected_output[0]); ++i)
    {
        ck_assert_int_eq(dest_begin[i], expected_output[i]);
    }

    free(dest_begin);
}
END_TEST

// Первый элемент - отрицательный
START_TEST(test_key_first_element_negative)
{
    const int input[] = {-1, 2, 3, 4, 5};
    int *dest_begin = NULL;
    int *dest_end = NULL;

    int result = key(input, input + sizeof(input) / sizeof(input[0]), &dest_begin, &dest_end);

    ck_assert_int_eq(result, ERR_FILTER_EMPTY);
    ck_assert_ptr_eq(dest_begin, NULL);
    ck_assert_ptr_eq(dest_end, NULL);
}
END_TEST

// нулевых указателей
START_TEST(test_key_null_pointers)
{
    int *pb_src = NULL;
    int *pe_src = NULL;
    int *pb_dst = NULL;
    int *pe_dst = NULL;

    int result = key(pb_src, pe_src, &pb_dst, &pe_dst);

    ck_assert_int_eq(result, ERR_FILTER_PARAM);
    ck_assert_ptr_null(pb_dst);
    ck_assert_ptr_null(pe_dst);
}
END_TEST
// неверные указатели тестового ключа
START_TEST(test_key_invalid_pointers)
{
    int src[] = {1, 2, 3, 4, 5};
    int *pb_dst = NULL;
    int *pe_dst = NULL;

    int result = key(src + 5, src, &pb_dst, &pe_dst);

    ck_assert_int_eq(result, ERR_FILTER_PARAM);
    ck_assert_ptr_null(pb_dst);
    ck_assert_ptr_null(pe_dst);
}
END_TEST
// все отрицательные
START_TEST(test_key_filter_all_negative)
{
    int src[] = {-1, -2, -3, -4, -5};
    int *pb_dst = NULL;
    int *pe_dst = NULL;

    int result = key(src, src + 5, &pb_dst, &pe_dst);

    ck_assert_int_eq(result, ERR_FILTER_EMPTY);
    ck_assert_ptr_null(pb_dst);
    ck_assert_ptr_null(pe_dst);
}
END_TEST
// нулевые указатели назначения
START_TEST(test_key_null_destination_pointers)
{
    int src[] = {1, -2, 3, -4, 5};
    int *pb_dst = NULL;
    int *pe_dst = NULL;

    int result = key(src, src + 5, NULL, NULL);

    ck_assert_int_eq(result, ERR_FILTER_PARAM);
    ck_assert_ptr_null(pb_dst);
    ck_assert_ptr_null(pe_dst);
}
END_TEST
// endregion

// region mysort

// Случайный порядок, все элементы разные
START_TEST(test_sort_random_order)
{
    int arr[] = {2, 3, 5, 4, 1};
    int expected[] = {1, 2, 3, 4, 5};

    mysort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_compare);

    ck_assert_mem_eq(arr, expected, sizeof(expected));
}
END_TEST

// Числа, упорядоченные по возрастанию
START_TEST(test_sort_sorted_ascending)
{
    int arr[] = {5, 6, 7, 8, 9, 10};
    mysort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_compare);
    int expected[] = {5, 6, 7, 8, 9, 10};
    ck_assert_mem_eq(arr, expected, sizeof(expected));
}
END_TEST
// четные числа, упорядоченные по возрастанию
START_TEST(test_sort_sorted_even_number_ascending)
{
    int arr[] = {2, 4, 6, 8, 10, 12};
    mysort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_compare);
    int expected[] = {2, 4, 6, 8, 10, 12};
    ck_assert_mem_eq(arr, expected, sizeof(expected));
}
END_TEST
// нечетные числа, упорядоченные по возрастанию
START_TEST(test_sort_sorted_odd_number_ascending)
{
    int arr[] = {1, 3, 5, 7, 9, 11};
    mysort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_compare);
    int expected[] = {1, 3, 5, 7, 9, 11};
    ck_assert_mem_eq(arr, expected, sizeof(expected));
}
END_TEST
// Числа, упорядоченные по убыванию
START_TEST(test_sort_sorted_descending)
{
    int arr[] = {10, 9, 8, 7, 6, 5};
    mysort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_compare);
    int expected[] = {5, 6, 7, 8, 9, 10};
    ck_assert_mem_eq(arr, expected, sizeof(expected));
}
END_TEST
// Четные числа, упорядоченные по убыванию
START_TEST(test_sort_sorted_even_number_descending)
{
    int arr[] = {10, 8, 6, 4, 2, 0};
    mysort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_compare);
    int expected[] = {0, 2, 4, 6, 8, 10};
    ck_assert_mem_eq(arr, expected, sizeof(expected));
}
END_TEST
// нечетные числа, упорядоченные по убыванию
START_TEST(test_sort_sorted_odd_number_descending)
{
    int arr[] = {11, 9, 7, 5, 3, 1};
    mysort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_compare);
    int expected[] = {1, 3, 5, 7, 9, 11};
    ck_assert_mem_eq(arr, expected, sizeof(expected));
}
END_TEST
// Случайный порядок, но есть одинаковые элементы
START_TEST(test_sort_only_duplicate_elements)
{
    int arr[] = {1, 1, 1, 1, 1};
    mysort(arr, sizeof(arr) / sizeof(arr[0]), sizeof(int), int_compare);
    int expected[] = {1, 1, 1, 1, 1};
    ck_assert_mem_eq(arr, expected, sizeof(expected));
}
END_TEST

// endregion

// region count_elements_in_file

// Пустой файл
START_TEST(test_count_empty_file)
{
    FILE *empty_file = fopen(FILE_TMP, "w");
    fclose(empty_file);

    FILE *f = fopen(FILE_TMP, "r");
    size_t n = 0;
    int result = count_elements_in_file(f, &n);

    ck_assert_int_eq(result, OK);
    ck_assert_uint_eq(n, 0);

    fclose(f);
}
END_TEST

// Файл с одним элементами
START_TEST(test_count_elements_one_element)
{
    FILE *single_element_file = fopen(FILE_TMP, "w");
    fprintf(single_element_file, "42");
    fclose(single_element_file);

    FILE *f = fopen(FILE_TMP, "r");
    size_t n = 0;
    int result = count_elements_in_file(f, &n);

    ck_assert_int_eq(result, OK);
    ck_assert_uint_eq(n, 1);

    fclose(f);
}
END_TEST
// Файл с несколькими элементами
START_TEST(test_count_elements_multiple_elements)
{
    FILE *multiple_elements_file = fopen(FILE_TMP, "w");
    fprintf(multiple_elements_file, "1 2 3 4 5");
    fclose(multiple_elements_file);

    FILE *f = fopen(FILE_TMP, "r");
    size_t n = 0;
    int result = count_elements_in_file(f, &n);

    ck_assert_int_eq(result, OK);
    ck_assert_uint_eq(n, 5);

    fclose(f);
}
END_TEST
Suite *my_lib_suite(void)
{
    Suite *suite = suite_create("my_lib");

    TCase *tc_pos = tcase_create("positives");
    TCase *tc_neg = tcase_create("negatives");

    // key
    tcase_add_test(tc_pos, test_key_no_negative_element);
    tcase_add_test(tc_pos, test_key_last_element_negative);
    tcase_add_test(tc_pos, test_key_middle_element_negative);
    tcase_add_test(tc_pos, test_key_multiple_negative_elements);
    tcase_add_test(tc_pos, test_key_two_negative_elements);
    tcase_add_test(tc_neg, test_key_first_element_negative);
    tcase_add_test(tc_neg, test_key_null_pointers);
    tcase_add_test(tc_neg, test_key_invalid_pointers);
    tcase_add_test(tc_neg, test_key_filter_all_negative);
    tcase_add_test(tc_neg, test_key_null_destination_pointers);
    // mysort
    tcase_add_test(tc_pos, test_sort_random_order);
    tcase_add_test(tc_pos, test_sort_sorted_ascending);
    tcase_add_test(tc_pos, test_sort_sorted_descending);
    tcase_add_test(tc_pos, test_sort_sorted_even_number_ascending);
    tcase_add_test(tc_pos, test_sort_sorted_odd_number_ascending);
    tcase_add_test(tc_pos, test_sort_sorted_even_number_descending);
    tcase_add_test(tc_pos, test_sort_sorted_odd_number_descending);
    tcase_add_test(tc_pos, test_sort_only_duplicate_elements);

    // count_elements_in_file
    tcase_add_test(tc_pos, test_count_empty_file);
    tcase_add_test(tc_pos, test_count_elements_one_element);
    tcase_add_test(tc_pos, test_count_elements_multiple_elements);

    suite_add_tcase(suite, tc_pos);
    suite_add_tcase(suite, tc_neg);
    return suite;
}
