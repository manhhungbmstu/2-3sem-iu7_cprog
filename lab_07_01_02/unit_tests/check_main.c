#include <stdlib.h>
#include <check.h>
#include "check_my_lib.h"

int main(void)
{
    int n_failed;
    SRunner *runner = srunner_create(NULL);
    srunner_add_suite(runner, my_lib_suite());

    // Тестируем
    srunner_run_all(runner, CK_NORMAL);
    n_failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    if (n_failed == 0)
    {
        return EXIT_SUCCESS;
    }
    else
    {
        return EXIT_FAILURE;
    }
}
