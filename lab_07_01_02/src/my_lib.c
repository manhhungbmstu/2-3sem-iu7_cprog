#include <assert.h>
#include <stdlib.h>

#include "my_lib.h"
#include "my_error.h"
#include <string.h>
#define ERR_FILTER_EMPTY -10
#define ERR_FILTER_MEM_ALLOC -11
#define ERR_FILTER_PARAM -14

#define OK 0
int key(const int *pb_src, const int *pe_src, int **dest_begin, int **dest_end)
{
    if (!pb_src || !pe_src || !dest_begin || !dest_end || pb_src >= pe_src)
        return ERR_FILTER_PARAM;

    // Ищем первый отрицательный элемент в исходном массиве
    const int *first_negative = pb_src;
    while (first_negative < pe_src && *first_negative >= 0)
    {
        first_negative++;
    }

    // Вычисляем количество элементов, удовлетворяющих условию
    size_t new_size = first_negative - pb_src;
    if (new_size == 0)
        return ERR_FILTER_EMPTY;

    *dest_begin = malloc(new_size * sizeof(int));
    if (!*dest_begin)
        return ERR_FILTER_MEM_ALLOC;

    for (size_t i = 0; i < new_size; i++)
    {
        (*dest_begin)[i] = pb_src[i];
    }
    // memcpy(*dest_begin, pb_src, new_size * sizeof(int));
    *dest_end = *dest_begin + new_size;

    return OK;
}

void swap(void *l, void *r, const size_t elem_size)
{
    void *temp = malloc(elem_size);

    if (temp == NULL)
    {
        return;
    }
    unsigned char *left = (unsigned char *)l;
    unsigned char *right = (unsigned char *)r;

    for (size_t i = 0; i < elem_size; i++)
    {
        // Обмениваем байты между left и right.
        unsigned char temp = left[i];
        left[i] = right[i];
        right[i] = temp;
    }

    free(temp);
}
void mysort(void *data, size_t n, size_t elem_size, compare_func_t cmp)
{
    if (n < 2)
        return;

    size_t swapped;
    do
    {
        swapped = 0;
        for (size_t i = 1; i < n; i++)
        {
            char *current = (char *)data + i * elem_size;
            char *prev = current - elem_size;

            if (cmp(prev, current) > 0)
            {
                swap(prev, current, elem_size);
                swapped = 1;
            }
        }
        n--;
    } while (swapped);
}

int count_elements_in_file(FILE *f, size_t *n)
{
    assert(f);

    int rc = OK;
    int count = 0;
    int tmp;

    while (fscanf(f, "%d", &tmp) == 1)
    {
        count++;
    }
    *n = count;

    if (feof(f))
        return OK;
    return rc;
}
