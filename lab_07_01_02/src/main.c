#include <stdio.h>
#include <string.h>
#include "my_error.h"
#include "array.h"
#include "my_lib.h"
#include "my_function.h"
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int rc = ERR_USAGE;

    if (argc != 3 && !(argc == 4 && strcmp(argv[3], ARG_FILTER) == 0))
    {
        err_print(rc);
        return rc;
    }

    FILE *input_file = fopen(argv[1], "r");
    if (!input_file)
    {
        rc = errno;
        err_print(rc);
        return rc;
    }

    FILE *output_file = fopen(argv[2], "w");
    if (!output_file)
    {
        rc = errno;
        err_print(rc);
        fclose(input_file);
        return rc;
    }

    size_t num_elements;
    rc = count_elements_in_file(input_file, &num_elements);

    if (num_elements == 0)
    {
        rc = ERR_FILE_EMPTY;
        err_print(rc);
        fclose(output_file);
        fclose(input_file);
        return rc;
    }

    int *data = malloc(num_elements * sizeof(int));
    if (!data)
    {
        rc = ERR_ARRAY_MEM_ALLOC;
        err_print(rc);
        fclose(output_file);
        fclose(input_file);
        return rc;
    }

    size_t max_elements = num_elements;
    rewind(input_file);
    rc = array_read(data, &num_elements, max_elements, input_file);

    if (rc)
    {
        err_print(rc);
        free(data);
        fclose(output_file);
        fclose(input_file);
        return rc;
    }

    if (argc == 4)
    {
        int *filtered_data_start = NULL;
        int *filtered_data_end = NULL;

        rc = key(data, data + num_elements, &filtered_data_start, &filtered_data_end);

        if (!rc)
        {
            mysort(filtered_data_start, filtered_data_end - filtered_data_start, sizeof(*filtered_data_start), int_compare);
            array_write(filtered_data_start, filtered_data_end - filtered_data_start, output_file, ' ');
            free(filtered_data_start);
        }
    }
    else
    {
        mysort(data, num_elements, sizeof(*data), int_compare);
        array_write(data, num_elements, output_file, ' ');
    }

    free(data);
    fclose(output_file);
    fclose(input_file);

    return rc;
}
