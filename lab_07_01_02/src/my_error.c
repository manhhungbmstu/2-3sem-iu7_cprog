#include <stdio.h>
#include <string.h>

#include "my_error.h"

void err_print(const int err_code)
{
    if (err_code > 0)
    {
        fprintf(stdout, "Error: %s\n", strerror(err_code));
        return;
    }

    switch (err_code)
    {
        case ERR_USAGE:
            fprintf(stdout, "app.exe IN_FILE OUT_FILE [f]\n");
            break;
        case OK:
            break;
        default:
            fprintf(stdout, "Error: %d (not named yet)!\n", err_code);
            break;
    }
}
