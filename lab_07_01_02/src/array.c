#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "my_function.h"
#include "array.h"
#include "my_error.h"
#define ERR_INVALID_ARGUMENT -30
#define ERR_MEMORY_ALLOCATION -31
int array_read(int data[], size_t *n, const size_t n_max, FILE *f)
{
    if (!data || !f)
    {
        return OK;
    }

    int rc = OK;
    int tmp;
    *n = 0;

    while (*n < n_max && (rc = int_read(&tmp, f)) == OK)
    {
        data[*n] = tmp;
        (*n)++;
    }

    if (feof(f))
    {
        return OK;
    }

    return rc;
}

void array_write(const int *data, const size_t n, FILE *f, const char delim)
{
    if (data == NULL || f == NULL)
    {
        fprintf(stderr, "Error: Invalid arguments to array_write\n");
        return;
    }

    if (n == 0)
    {
        // Нет данных для записи, просто возвращаемся
        return;
    }

    for (size_t i = 0; i < n; i++)
    {
        fprintf(f, "%d", data[i]);

        if (i < n - 1)
        {
            fprintf(f, "%c", delim);
        }
    }

    fprintf(f, "\n");
}
