#include "my_function.h"
#include "my_error.h"
#include <math.h>
int int_read(void *data, FILE *f)
{
    int *num = data;

    if (fscanf(f, "%d", num) != 1)
    {
        return ERR_INT_IO;
    }

    return OK;
}

int int_compare(const void *l, const void *r)
{
    const int *pl = (const int *)l;
    const int *pr = (const int *)r;

    if (*pl < *pr)
        return -1;
    else if (*pl > *pr)
        return 1;
    else
        return 0;
}
