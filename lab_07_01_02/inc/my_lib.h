#ifndef MY_LIB_H__
#define MY_LIB_H__

#include <stddef.h>
#include "callback.h"

#define ARG_FILTER "f"

/**
 * @brief Функция фильт (вариант 2)
 *
 * В массиве остаются элементы от нулевого до m-го,
 * где m - индекс первого отрицательного элемента этого массива либо
 * число n (размер исходного массива), если такого элемента в массиве нет.
 * Т.е. отфильтрованный массив содержит элементы,
 * расположенные перед первым отрицательным элементом, или весь исходный массив,
 * если отрицательные элементы отсутствуют.
 */
int key(const int *pb_src, const int *pe_src, int **pb_dst, int **pe_dst);

/**
 * @brief Сортировать (вариант 7)
 */
void mysort(void *data, size_t n, size_t elem_size, compare_func_t cmp);

/**
 * @brief Подсчитать количество элементов в файле
 *
 * @param [in,out] f Файл
 * @param [out] n Количество элементов
 * @param [in] elem_size Размер элемента
 * @param [in] read Функция чтения
 * @return int Код ошибки
 */
/**
 * @brief Обмен значения
 */
void swap(void *l, void *r, const size_t elem_size);
int count_elements_in_file(FILE *f, size_t *n);
#endif
