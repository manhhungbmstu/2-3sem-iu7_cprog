#ifndef CALLBACK_H__
#define CALLBACK_H__

#include <stddef.h>
#include <stdio.h>

/**
 * @brief Тип функция сравнения
 */
typedef int (*compare_func_t)(const void *pl, const void *pr);

/**
 * @brief Тип функция обмена
 */
typedef void (*swap_func_t)(void *pl, void *pr, size_t elem_size);

/**
 * @brief Тип функция чтения
 */
typedef int (*read_func_t)(void *data, FILE *f);

/**
 * @brief Тип функция запись
 */
typedef void (*write_func_t)(const void *data, FILE *f);

#endif
