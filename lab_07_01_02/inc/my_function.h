#ifndef MY_INTEGER_H__
#define MY_INTEGER_H__
#define EPSILON (1e-6)
#include <stdio.h>
int int_read(void *data, FILE *f);

int int_compare(const void *l, const void *r);

#endif
