#ifndef ARRAY_H__
#define ARRAY_H__

#include <stddef.h>
#include <stdio.h>

#include "callback.h"

/**
 * @brief Чтение значения в массив
 */
int array_read(int *data, size_t *n, const size_t n_max, FILE *f);

/**
 * @brief Вывод значения массива в файл
 */
void array_write(const int *data, const size_t n, FILE *f, const char delim);

/**
 * @brief Подсчитать количество элементов в файле
 */
int count_elements_in_file(FILE *f, size_t *n);

#endif
