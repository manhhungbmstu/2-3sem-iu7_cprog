#ifndef ERROR_H__
#define ERROR_H__

#include <errno.h>

#define OK 0

#define ERR_USAGE -1

#define ERR_INT_IO -2

#define ERR_FLOAT_IO -3

#define ERR_STR_IO -4
#define ERR_STR_RANGE -6
#define ERR_STR_MEM_ALLOC -7

#define ERR_MOVIE_IO -5

#define ERR_ARRAY_IO -8
#define ERR_ARRAY_RANGE -9
#define ERR_ARRAY_MEM_ALLOC -12

#define ERR_FILTER_EMPTY -10
#define ERR_FILTER_MEM_ALLOC -11
#define ERR_FILTER_PARAM -14

#define ERR_FILE_EMPTY -13

/**
 * @brief Вывод сообщения об ошибки
 *
 * @param err_code Код ошибки
 */
void err_print(const int err_code);

#endif
