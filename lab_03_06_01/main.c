#include <stdio.h>
#include <stdlib.h>
#include "array.h"

void fill_spiral(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    int num = 1;
    int start_row = 0, end_row = row - 1;
    int start_col = 0, end_col = col - 1;

    while (start_row <= end_row && start_col <= end_col)
    {
        for (int j = start_col; j <= end_col; j++) 
            a[start_row][j] = num++;

        for (int i = start_row + 1; i <= end_row; i++)
            a[i][end_col] = num++;

        if (start_row < end_row && start_col < end_col)
        {
            for (int j = end_col - 1; j > start_col; j--)
                a[end_row][j] = num++;

            for (int i = end_row; i > start_row; i--)
                a[i][start_col] = num++;
        }

        start_row++;
        end_row--;
        start_col++;
        end_col--;
    }
}
int main(void)
{
    size_t row = 0;
    size_t col = 0;
    int a[N_MAX][N_MAX];

    if (input_number_of_matrix(&row, &col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }
    if (row != col)
    {
        printf("Ошибка: не квадратичная матрица\n");
        return ERR;
    }

    fill_spiral(a, row, col);

    output_array(a, row, col);

    return OK;
}
