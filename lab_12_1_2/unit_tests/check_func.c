#include <stdlib.h>

#include "check_func.h"
#include "func.h"

START_TEST(test_rotate)
{
    int arr[] = {1, 2, 3, 4, 5};
    int len = sizeof(arr) / sizeof(arr[0]);
    int k = 2;

    left_rotate(arr, len, k);

    // Check the rotated array
    ck_assert_int_eq(arr[0], 3);
    ck_assert_int_eq(arr[1], 4);
    ck_assert_int_eq(arr[2], 5);
    ck_assert_int_eq(arr[3], 1);
    ck_assert_int_eq(arr[4], 2);
}
END_TEST

// Оценка максимально возможного размера массива и выделить память с запасом
START_TEST(test_filter)
{
    int src[] = {1, 2, 3, 4, 5, 16, 25};
    size_t n_src = sizeof(src) / sizeof(src[0]);
    int dst[10]; 
    size_t n_dst;

    filter_square(src, n_src, dst, &n_dst);

    ck_assert_int_eq(n_dst, 4); 
    ck_assert_int_eq(dst[0], 1); 
    ck_assert_int_eq(dst[1], 4); 
    ck_assert_int_eq(dst[2], 16); 

}
END_TEST


Suite *func_suite(void)
{
    Suite *suite = suite_create("func");

    TCase *tc_case = tcase_create("");

    tcase_add_test(tc_case, test_rotate);

    tcase_add_test(tc_case, test_filter);


    suite_add_tcase(suite, tc_case);
    return suite;
}
