#include <math.h>
#include "func.h"

void left_rotate(int *arr, int len, int k)
{
    for (int i = 0; i < k; i++)
    {
        int temp = arr[0];
        for (int i = 0; i < len - 1; i++)
            arr[i] = arr[i + 1];
        arr[len - 1] = temp;
    }
}

// Проверка является ли число квадрато
static int is_square(int x)
{
    int s = (int)sqrt((double)x);
    return s * s == x;
}

void filter_square(const int *src, const size_t n_src, int *dst, size_t *n_dst)
{
    *n_dst = 0;

    for (size_t i = 0; i < n_src; i++)
    {
        if (is_square(src[i]))
        {
            if (dst)
                dst[(*n_dst)++] = src[i];
        }
    }
}
