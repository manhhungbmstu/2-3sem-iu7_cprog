import tkinter as tk
from tkinter import messagebox
import ctypes

# Load the library
lib = ctypes.CDLL('./out/libmylib.so')

# Define functions to work with the library
def left_rotate(arr, k):
    _left_rotate = lib.left_rotate
    _left_rotate.argtypes = [ctypes.POINTER(ctypes.c_int), ctypes.c_int, ctypes.c_int]
    _left_rotate.restype = None

    arr = (ctypes.c_int * len(arr))(*arr)
    _left_rotate(arr, len(arr), k)

    return list(arr)

def filter_square(src):
    _filter_square = lib.filter_square
    _filter_square.argtypes = [ctypes.POINTER(ctypes.c_int), ctypes.c_int,
                                ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int)]
    _filter_square.restype = None

    n_dst = ctypes.c_int()
    dst = (ctypes.c_int * len(src))()

    _filter_square((ctypes.c_int * len(src))(*src), len(src), dst, n_dst)

    return list(dst)[:n_dst.value]

def filter_square2(src):
    _filter_square = lib.filter_square
    _filter_square.argtypes = [ctypes.POINTER(ctypes.c_int), ctypes.c_int,
                                ctypes.POINTER(ctypes.c_int), ctypes.POINTER(ctypes.c_int)]
    _filter_square.restype = None

    n_dst = ctypes.c_int()
    _filter_square((ctypes.c_int * len(src))(*src), len(src), None, n_dst)

    dst = (ctypes.c_int * n_dst.value)()
    _filter_square((ctypes.c_int * len(src))(*src), len(src), dst, n_dst)

    return list(dst)

# Functions to handle button events
def perform_left_rotate():
    try:
        array = [int(x) for x in entry_array.get().split()]
        k = int(entry_k.get())

        result = left_rotate(array, k)

        label_result.config(text='Rotated Array: ' + ' '.join(map(str, result)), fg='green')
    except ValueError:
        messagebox.showerror("Ошибка", "Некорректный ввод")

def perform_filter_square():
    try:
        array = [int(x) for x in entry_array.get().split()]

        result = filter_square(array)

        label_result.config(text='Filtered Array: ' + ' '.join(map(str, result)), fg='blue')
    except ValueError:
        messagebox.showerror("Ошибка", "Некорректный ввод")

def perform_filter_square2():
    try:
        array = [int(x) for x in entry_array.get().split()]

        result = filter_square2(array)

        label_result.config(text='Filtered Array: ' + ' '.join(map(str, result)), fg='red')
    except ValueError:
        messagebox.showerror("Ошибка", "Некорректный ввод")

# Create the main window
root = tk.Tk()
root.title("Array Operations")

# Create frames
input_frame = tk.Frame(root, bg='lightgray')
input_frame.pack(pady=10)

button_frame = tk.Frame(root, bg='lightgray')
button_frame.pack(pady=10)

result_frame = tk.Frame(root, bg='lightgray')
result_frame.pack(pady=10)

# Elements for input
tk.Label(input_frame, text="Введите массив чисел:", bg='lightgray', fg='black').grid(row=0, column=0, padx=5, pady=5)
entry_array = tk.Entry(input_frame)
entry_array.grid(row=0, column=1, padx=5, pady=5)

tk.Label(input_frame, text="Введите число k для сдвига:", bg='lightgray', fg='black').grid(row=1, column=0, padx=5, pady=5)
entry_k = tk.Entry(input_frame)
entry_k.grid(row=1, column=1, padx=5, pady=5)

# Buttons for operations
tk.Button(button_frame, text="Выполнить циклический сдвиг", command=perform_left_rotate, bg='green', fg='white').pack(side=tk.LEFT, padx=5)
tk.Button(button_frame, text="Фильтровать полные квадраты", command=perform_filter_square, bg='blue', fg='white').pack(side=tk.LEFT, padx=5)
tk.Button(button_frame, text="Фильтровать полные квадраты 2", command=perform_filter_square2, bg='black', fg='white').pack(side=tk.LEFT, padx=5)

# Display result
label_result = tk.Label(result_frame, text="Результат будет здесь", bg='lightgray', fg='black')
label_result.pack()

# Start the main loop
root.mainloop()
