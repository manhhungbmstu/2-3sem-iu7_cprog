#ifndef FUNC_H__
#define FUNC_H__

#include <stddef.h>

/**
 * @brief Функция циклического сдвига массива на k позиций влево
 *
 * @param arr Массив
 * @param len Количество элементов
 * @param k На сколько сдвинуть?
 */
void left_rotate(int *arr, int len, int k);

/**
 * @brief Функция, которая из одного массива помещает в другой элементы первого,
 * которые являются полными квадратами.
 *
 * @param src Исходный массив
 * @param n_src Количество элементов исходного массива
 * @param dst Полученный массив
 * @param n_dst Количество элементов полученного массива
 */
void filter_square(const int *src, const size_t n_src, int *dst, size_t *n_dst);

#endif