#!/bin/bash

input_file="$1"

# Проверяем, что файл существует
if [ ! -f "$input_file=" ]; then
    echo "Error: file '$input_file=' not found" >&2
    exit 1
fi

# Запускаем программу app.exe с вводом из файла и выводом в /dev/null
if ! ../../app.exe < "$input_file=" > /dev/null 2>&1; then
  exit 0
else
  exit 1
fi