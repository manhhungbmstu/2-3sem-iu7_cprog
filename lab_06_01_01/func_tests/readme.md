# Тесты для лабораторной работы № 06, Вариант 01

## Входные данные

Файл с информацией о кинофильмах, которая включает в себя название кинофильма, фамилию режиссёра и год выхода.

## Выходные данные

Информация о кинофильмах, которая включает в себя название кинофильма, фамилию режиссёра и год выхода. ИЛИ «Not found».

## Позитивные тесты

### Без повторения

- 01 - FIELD 'название кинофильма' без KEY
- 02 - FIELD 'название кинофильма' с KEY

- 21 - FIELD 'фамилию режиссёра' без KEY
- 22 - FIELD 'фамилию режиссёра' с KEY

- 41 - FIELD 'год выхода' без KEY
- 42 - FIELD 'год выхода' с KEY

### C повторения

- 11 - FIELD 'название кинофильма' без KEY
- 12 - FIELD 'название кинофильма' с KEY

- 31 - FIELD 'фамилию режиссёра' без KEY
- 32 - FIELD 'фамилию режиссёра' с KEY

- 51 - FIELD 'год выхода' без KEY
- 52 - FIELD 'год выхода' с KEY

### Крайние

- 71 - Одна запись
- 72 - 15 записи
- 73 - 14 записи

## Негативные тесты

- 01 - длина KEY 'название кинофильма' > 25
- 02 - длина 'название кинофильма' > 25
- 03 - длина KEY 'название кинофильма' = 0 

- 04 - длина KEY 'фамилию режиссёра' > 25
- 05 - длина 'фамилию режиссёра' > 25
- 06 - длина 'фамилию режиссёра' = 0

- 07 - 'год выхода' <= 0
- 08 - 'год выхода' не число
- 09 - KEY year <= 0

- 10 - Нуль запись
- 11 - 16 записи
