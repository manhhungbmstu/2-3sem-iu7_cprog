#!/bin/bash

# Build program with debugging information
# with address sanitizer

./build_debug.sh -fsanitize=address
