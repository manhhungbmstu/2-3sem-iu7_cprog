#!/bin/bash

# Clean binaries, artifacts
#

rm ./*.exe ./*.out ./*.o ./*.gcno ./*.gcda ./*.gcov ./*.prof* >/dev/null 2>&1
