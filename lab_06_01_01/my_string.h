#ifndef MY_STRING_H__
#define MY_STRING_H__

/**
 * Читать строку с проверкой диапазона
 */
int str_read(char *data, FILE *f, const size_t n_max, char *buf, const size_t buf_size);

#endif
