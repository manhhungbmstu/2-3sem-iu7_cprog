#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "error.h"
#include "array.h"
#include "movie.h"

int main(int argc, char **argv)
{
    int rc = ERR_USAGE;
    FILE *file;
    movie_t movies[N_MAX];
    size_t n;
    enum cmp_e cmp;

    // проверка количество аргументов
    if (argc >= 3 && argc <= 4)
    {
        rc = OK;
        // открываем файл
        file = fopen(argv[1], "r");
        if (!file)
        {
            rc = errno;
            err_print(rc);
            return rc;
        }
        // Объявляем  target
        movie_t target;
        // Перемещаем объявление len в начало блока
        size_t len; 
        // Выбираем подходящий компаратор
        switch (argc)
        {
            case 3:
                if (strcmp(argv[2], "title") == 0)
                    cmp = CMP_BY_TITLE;
                else if (strcmp(argv[2], "name") == 0)
                    cmp = CMP_BY_NAME;
                else if (strcmp(argv[2], "year") == 0)
                    cmp = CMP_BY_YEAR;
                else
                    rc = ERR_USAGE;
                break;
            case 4:
                len = strlen(argv[3]);
                if (strcmp(argv[2], "title") == 0 && len <= TITLE_LEN && len > 0)
                {
                    cmp = CMP_BY_TITLE;
                    strncpy(target.title, argv[3], sizeof(target.title));
                }
                else if (strcmp(argv[2], "name") == 0 && len <= NAME_LEN && len > 0)
                {
                    cmp = CMP_BY_NAME;
                    strncpy(target.name, argv[3], sizeof(target.name));
                }
                else if (strcmp(argv[2], "year") == 0)
                {
                    cmp = CMP_BY_YEAR;
                    char *endptr;
                    int year = strtol(argv[3], &endptr, 10);
                    if (*endptr == 0 && year >= MIN_YEAR && year <= MAX_YEAR)
                        target.year = year;
                    else
                        rc = ERR_USAGE;
                }
                else
                    rc = ERR_USAGE;
                break;
            default:
                rc = ERR_USAGE;
        }
        if (!rc)
        {
            rc = array_read_sorted(movies, sizeof(movies) / sizeof(movies[0]), &n, sizeof(movies[0]), file, cmp);
            if (n == 0)
                rc = ERR_ARRAY_RANGE;
            if (!rc)
            {
                if (argc == 3)
                {
                    rc = write_array(movies, sizeof(movies) / sizeof(movies[0]), n, stdout);
                }
                else
                {
                    int idx = array_search_binary(&target, movies, sizeof(movies) / sizeof(movies[0]), n, cmp);
                    if (idx == -1)
                    {
                        printf("Not found\n");
                    }
                    else
                    {
                        rc = movie_write(&movies[idx], stdout);
                    }
                }
            }
        }

        fclose(file);
    }

    if (rc)
        err_print(rc);
    return rc;
}
