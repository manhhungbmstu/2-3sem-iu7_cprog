#ifndef ARRAY_H__
#define ARRAY_H__

#include <stdio.h>
#include "movie.h"

/**
 * Читать в массив в отсортированном месте
 */
int array_read_sorted(movie_t *array, const size_t n_max, size_t *n, const size_t elem_size, FILE *f, enum cmp_e cmp);

/**
 * @brief Вывод элемента в файл
 * @return int Код ошибки
 */
int write_array(const movie_t *array, const size_t n_max, const size_t n, FILE *f);

/**
 * @brief Бинарный поиск элемента в массиве
 */
int array_search_binary(const movie_t *target, const movie_t *array, const size_t n_max, const size_t n, enum cmp_e cmp);

#endif
