#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "error.h"
#include "my_string.h"
#define ERR_INVALID_ARGUMENT -10
#include <stddef.h>

int str_read(char *data, FILE *f, const size_t n_max, char *buf, const size_t buf_size)
{
    if (data == NULL || f == NULL)
    {
        return ERR_INVALID_ARGUMENT;
    }

    assert(buf_size > n_max);

    if (fgets(buf, buf_size, f) == NULL)
    {
        return ERR_STR_IO;
    }

    size_t len = strcspn(buf, "\n");

    if (len > 0 && buf[len - 1] == '\n')
    {
        buf[len - 1] = '\0';
        len--;
    }

    if (len == 0 || len >= n_max)
    {
        return ERR_STR_RANGE;
    }

    strncpy(data, buf, len);
    data[len] = '\0';

    return OK;
}
