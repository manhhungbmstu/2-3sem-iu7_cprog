#include <stdio.h>
#include <assert.h>

#include "error.h"
#include "my_integer.h"

int int_read(int *data, FILE *f, const int value_min, const int value_max)
{
    assert(value_min <= value_max);

    int num;

    if (fscanf(f, "%d", &num) != 1)
    {
        return ERR_INT_IO;
    }

    if (num < value_min || num > value_max)
    {
        return ERR_INT_RANGE;
    }

    *data = num;
    return OK;
}
