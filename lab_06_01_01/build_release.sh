#!/bin/bash
# Build program for release
#

gcc \
    -std=c99 -Wall -Werror -Wextra -Wpedantic \
    -Wfloat-equal -Wfloat-conversion \
    -Wvla \
    -D NDEBUG \
    -c ./*.c

gcc \
    -o app.exe ./*.o \
    -lm
