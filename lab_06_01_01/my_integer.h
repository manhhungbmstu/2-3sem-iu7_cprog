#ifndef MY_INTEGER_H__
#define MY_INTEGER_H__

/**
 *Читать целое число с проверкой диапазона
 */
int int_read(int *data, FILE *f, const int value_min, const int value_max);

#endif
