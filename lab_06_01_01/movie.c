#include <string.h>

#include "movie.h"
#include "error.h"
#include "my_string.h"
#include "my_integer.h"

int movie_read(movie_t *ptr, FILE *f)
{
    int rc = OK;

    char buf_title[TITLE_LEN + 2];
    rc = str_read(ptr->title, f, TITLE_LEN, buf_title, sizeof(buf_title));
    if (rc)
        return rc;

    char buf_name[NAME_LEN + 2];
    rc = str_read(ptr->name, f, NAME_LEN, buf_name, sizeof(buf_name));
    if (rc)
        return rc;

    rc = int_read(&(ptr->year), f, MIN_YEAR, MAX_YEAR);

    char tmp[2];
    fgets(tmp, sizeof(tmp), f);

    return rc;
}

int movie_write(const movie_t *ptr, FILE *f)
{
    int rc = OK;

    if (fprintf(f, "%s\n", ptr->title) < 0 ||
        fprintf(f, "%s\n", ptr->name) < 0 ||
        fprintf(f, "%d\n", ptr->year) < 0)
    {
        rc = ERR_WRITE;
    }

    return rc;
}

int movie_cmp_by_title(const movie_t *pl, const movie_t *pr)
{
    const char *title1 = pl->title;
    const char *title2 = pr->title;

    while (*title1 != '\0' && *title2 != '\0')
    {
        if (*title1 < *title2)
        {
            return -1;
        }
        else if (*title1 > *title2)
        {
            return 1;
        }
        title1++;
        title2++;
    }

    if (*title1 == '\0' && *title2 == '\0')
    {
        return 0;
    }
    else if (*title1 == '\0')
    {
        // Title 1 is shorter, so it comes before Title 2
        return -1;
    }
    else
    {
        // Title 2 is shorter, so it comes before Title 1
        return 1;
    }
}
int movie_cmp_by_name(const movie_t *pl, const movie_t *pr)
{
    const char *name1 = pl->name;
    const char *name2 = pr->name;

    while (*name1 != '\0' && *name2 != '\0')
    {
        if (*name1 < *name2)
        {
            return -1;
        }
        else if (*name1 > *name2)
        {
            return 1;
        }
        name1++;
        name2++;
    }

    if (*name1 == '\0' && *name2 == '\0')
    {
        return 0;
    }
    else if (*name1 == '\0')
    {
        // Title 1 is shorter, so it comes before Title 2
        return -1;
    }
    else
    {
        // Title 2 is shorter, so it comes before Title 1
        return 1;
    }
}
int movie_cmp_by_year(const movie_t *pl, const movie_t *pr)
{
    if (pl->year < pr->year)
    {
        return -1;
    }
    else if (pl->year > pr->year)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
