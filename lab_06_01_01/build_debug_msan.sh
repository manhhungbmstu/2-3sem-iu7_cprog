#!/bin/bash

# Build program with debugging information
# with memory sanitizer

./build_debug.sh -fsanitize=leak
