#include <assert.h>
#include <string.h>

#include "array.h"
#include "error.h"
#define ERR_INVALID_ARGUMENT -10
typedef int (*cmp_func_t)(const movie_t *, const movie_t *);

static int cmp_func(const movie_t *pl, const movie_t *pr, enum cmp_e cmp)
{
    static cmp_func_t cmp_functions[] = {
        [CMP_BY_TITLE] = movie_cmp_by_title,
        [CMP_BY_NAME] = movie_cmp_by_name,
        [CMP_BY_YEAR] = movie_cmp_by_year};

    cmp_func_t cmp_function = cmp_functions[cmp];

    return (cmp_function) ? cmp_function(pl, pr) : OK;
}

int array_read_sorted(movie_t *array, const size_t n_max, size_t *n, const size_t elem_size, FILE *f, enum cmp_e cmp)
{
    int rc = OK;
    *n = 0;
    movie_t tmp;

    while (*n < n_max && (rc = movie_read(&tmp, f)) == OK)
    {
        assert(*n < n_max);

        if (*n == 0)
        {
            memcpy(array, &tmp, elem_size);
        }
        else
        {
            size_t i = *n;
            while (i > 0 && cmp_func(array + i - 1, &tmp, cmp) > 0)
            {
                array[i] = array[i - 1];
                i--;
            }
            array[i] = tmp;
        }

        (*n)++;
    }

    return (feof(f)) ? OK : ERR_ARRAY_RANGE;
}

int write_array(const movie_t *array, const size_t n_max, const size_t n, FILE *f)
{
    if (array == NULL || f == NULL || n > n_max)
    {
        return ERR_INVALID_ARGUMENT;
    }

    int result = OK;

    for (size_t i = 0; i < n; i++)
    {
        result = movie_write(array + i, f);

        if (result != OK)
        {
            break;
        }
    }

    return result;
}

int array_search_binary(const movie_t *target, const movie_t *array, const size_t n_max, const size_t n, enum cmp_e cmp)
{
    if (target == NULL || array == NULL || n > n_max)
    {
        return -1;
    }

    size_t begin = 0;
    size_t end = n;

    while (begin < end)
    {
        size_t mid = begin + (end - begin) / 2;
        const movie_t *mid_elem = array + mid;

        int result = cmp_func(mid_elem, target, cmp);

        if (result == 0)
        {
            return mid;
        }
        else if (result < 0)
        {
            begin = mid + 1;
        }
        else
        {
            end = mid;
        }
    }

    return -1;
}
