#ifndef MOVIE_H__
#define MOVIE_H__

#include <stdio.h>

#define TITLE_LEN 25
#define NAME_LEN 25

#define MIN_YEAR 1
#define MAX_YEAR 2099

#define N_MAX 15

typedef struct
{
    char title[TITLE_LEN + 1];
    char name[NAME_LEN + 1];
    int year;
} movie_t;

enum cmp_e
{
    CMP_BY_TITLE,
    CMP_BY_NAME,
    CMP_BY_YEAR
};

/**
Чтение структуру
 */
int movie_read(movie_t *ptr, FILE *f);
/**
Вывод структуру
 */
int movie_write(const movie_t *ptr, FILE *f);

/**
Сранение по 'название кинофильма'
 */
int movie_cmp_by_title(const movie_t *pl, const movie_t *pr);
/**
Сранение по 'фамилия режиссёра'
 */
int movie_cmp_by_name(const movie_t *pl, const movie_t *pr);
/**
Сранение по 'год выхода'
 */
int movie_cmp_by_year(const movie_t *pl, const movie_t *pr);

#endif
