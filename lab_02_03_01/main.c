#include <stdio.h>
#define N 20
#define OK 0
#define ERR -1

int fib(int p)
{
    if (p == 1)
        return 0;
    if (p == 2)
        return 1;
    return fib(p - 1) + fib(p - 2);
}
int print_array(const int arr[], size_t size)
{
    printf("Array element:\n");
    for (size_t i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return OK;
}
int input_array(int arr[], size_t size)
{
    printf("Input element:\n");
    for (size_t i = 0; i < size; i++)
    {
        if (scanf("%d", &arr[i]) != 1)
        {
            printf("Ошибка: введено не число\n");
            return ERR;
        }
    }
    return OK;
}
int main()
{
    int p = 1;
    int arr[N];
    size_t size;
    printf("Введите размер массива 1-10: ");
    if (scanf("%zu", &size) != 1)
    {
        printf("Ошибка: введено не число\n");
        return ERR;
    }
    if (size == 0)
    {
        printf("Ошибка: размер массива равен 0\n");
        return ERR;
    }
    if (size > 10)
    {
        printf("Ошибка: размер массива неправильный\n");
        return ERR;
    }
    if (input_array(arr, size) != 0)
    {
        return ERR;
    }
    setbuf(stdout, NULL);
    for (size_t i = 0; i < size; i++)
        if (arr[i] % 3 == 0)
        {
            for (size_t j = size - 1; j > i; j--)
                arr[j + 1] = arr[j];
            arr[i + 1] = fib(p);
            p++;
            size++;
            i++;
        }
    print_array(arr, size);
    return OK;
}
