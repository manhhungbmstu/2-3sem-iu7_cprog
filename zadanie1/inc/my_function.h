#ifndef MY_FUNCTION_H
#define MY_FUNCTION_H
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <math.h>
void inputArray(int **arr, int n);
void printArray(int *arr, int size);
void removePerfectSquares(int *arr, int *size);

#endif
