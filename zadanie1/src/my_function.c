#include "../inc/my_function.h"
int isPerfectSquare(int num)
{
    int sqrtNum = sqrt(num);
    return (sqrtNum * sqrtNum == num);
}

void removePerfectSquares(int *arr, int *size)
{
    int newSize = 0;
    for (int i = 0; i < *size; i++)
    {
        if (!isPerfectSquare(arr[i]))
        {
            arr[newSize] = arr[i];
            newSize++;
        }
    }
    *size = newSize;
}

void inputArray(int **arr, int n)
{
    *arr = (int *)malloc(n * sizeof(int));
    printf("Введите %d элементов массива:\n", n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &(*arr)[i]);
    }
}

void printArray(int *arr, int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}