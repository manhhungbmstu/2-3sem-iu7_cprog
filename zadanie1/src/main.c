#include "../inc/my_function.h"
#include "../inc/error.h"

#include <stdio.h>
#include <math.h>
#include <stdlib.h>



int main() {
    int n;
    printf("Введите размер массива: ");
    scanf("%d", &n);

    int *arr;
    inputArray(&arr, n);
    printf("масив: \n");
    printArray(arr, n);
    removePerfectSquares(arr, &n);
    printf("новый масиив: \n");
    printArray(arr, n);

    free(arr);

    return 0;
}
