#include <stdio.h>
#define N 10
#define OK 0
#define ERR 1
int input_element_array(int *p_first, int *p_last)
{
    int num_read = 0;
    for (int *p = p_first; p <= p_last; p++)
    {
        printf("Введите элемент %ld: ", p - p_first + 1);
        if (scanf("%d", p) == 1)
        {
            num_read++;
        }
        else
        {
            break;
        }
    }
    if (num_read == (p_last - p_first + 1))
    {
        return OK;
    }
    else
    {
        return ERR;
    }
}
int main()
{
    int arr[N];
    size_t n = sizeof(arr) / sizeof(arr[0]);
    int *p_first = arr;
    int *p_last = arr + n - 1;
    int max_sum = -100000000;
    printf("Ввведите размер: ");
    if (scanf("%zu", &n) == 1)
    {
        if ((n > 0) && (n < 11))
        {
            p_last = arr + n - 1;
            if (input_element_array(p_first, p_last) != 1)
            {
                while (p_first <= p_last)
                {
                    int sum = *p_first + *p_last;
                    if (sum > max_sum)
                    {
                        max_sum = sum;
                    }
                    p_first++;
                    p_last--;
                }
                printf("Максимальная сумма: %d\n", max_sum);
                return 0;
            }
            else
            {
                printf("Ошибка\n");
                return ERR;
            }
        }
        else
        {
            printf("Неправильный размер массива\n");
            return ERR;
        }
    }
    else
    {
        printf("Неправильно ввод размер\n");
        return ERR;
    }
}
