#include <stdio.h>
#include <stdlib.h>
#define OK 0
#define ERR 1
#define N 10

int is_prime(int n)
{
    if (n < 2)
    {
        return 1;
    }
    for (int i = 2; i * i <= n; i++)
    {
        if (n % i == 0)
        {
            return 1;
        }
    }
    return 0;
}
int input_array(int arr[], size_t size)
{
    printf("Input element:\n");
    for (size_t i = 0; i < size; i++)
    {
        if (scanf("%d", &arr[i]) != 1)
        {
            printf("Ошибка: введено не число\n");
            return ERR;
        }
    }
    return OK;
}

int print_array(const int arr[], size_t size)
{
    printf("Array elemet:\n");
    for (size_t i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return OK;
}

int main()
{
    int arr[N];
    size_t size;

    printf("Введите размер массива 1-10: ");
    if (scanf("%zu", &size) != 1)
    {
        printf("Ошибка: введено не число\n");
        return ERR;
    }

    if (size == 0)
    {
        printf("Ошибка: размер массива равен 0\n");
        return ERR;
    }

    if (size > 10)
    {
        printf("Ошибка: размер массива неправильный\n");
        return ERR;
    }

    if (input_array(arr, size) != 0)
    {
        return ERR;
    }

    int primes[N];
    size_t num_primes = 0;
    for (size_t i = 0; i < size; i++)
    {
        if (!is_prime(arr[i]))
        {
            primes[num_primes] = arr[i];
            num_primes++;
        }
    }

    if (num_primes == 0)
    {
        printf("Ошибка: в массиве нет простых чисел\n");
        return ERR;
    }
    print_array(primes, num_primes);
    return OK;
}
