#include <stdio.h>
#include <stdlib.h>
#include "array.h"


void find_num(int a[N_MAX][N_MAX], size_t row, size_t col, int *minrow, int *mincol);
int get_digit_sum(int n);
int main(void)
{
    size_t row = 0;
    size_t col = 0;
    int a[N_MAX][N_MAX];
    int minrow, mincol;

    if (input_number_of_matrix(&row, &col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }
    if (input_array(a, row, col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }
    find_num(a, row, col, &minrow, &mincol);
    if (delete_row(minrow, a, &row, &col) != OK)
    {
        return ERR;
    }

    if (delete_column(mincol, a, &row, &col) != OK)
    {
        return ERR;
    }
    if (row == 0 || col == 0)
    {
        printf("Нет элемента\n");
        return ERR_NO_ELEMENT;
    }

    output_array(a, row, col);

    return OK;
}
int get_digit_sum(int n)
{
    int sum = 0;
    n = abs(n);
    while (n > 0)
    {
        sum += abs(n % 10);
        n /= 10;
    }

    return sum;
}

void find_num(int a[N_MAX][N_MAX], size_t row, size_t col, int *minrow, int *mincol)
{
    // пусть 1-ый элемент - минимальный
    *minrow = 0;
    *mincol = 0;
    int min_digit_sum = get_digit_sum(a[0][0]);

    for (size_t i = 0; i < row; i++)
    {
        for (size_t j = 0; j < col; j++)
        {
            int sum = get_digit_sum(a[i][j]);
            if (sum < min_digit_sum)
            {
                min_digit_sum = sum;
                *minrow = i;
                *mincol = j;
            }
        }
    }
}

