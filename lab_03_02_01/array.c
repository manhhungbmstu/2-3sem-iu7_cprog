#include "array.h"

int input_number_of_matrix(size_t *row, size_t *col)
{
    printf("Введите номер строки: ");
    if (scanf("%zu", row) != 1)
    {
        printf("Error: Ввод/Вывод\n");
        return ERR;
    }
    if (*row <= 0 || *row > N_MAX)
    {
        printf("Ошибка: значение строка вне допустимого диапазона\n");
        return ERR_OUT_RANGE;
    }

    printf("Введите номер столбики: ");
    if (scanf("%zu", col) != 1)
    {
        printf("Error: Ввод/Вывод\n");
        return ERR;
    }
    if (*col <= 0 || *col > N_MAX)
    {
        printf("Ошибка: значение столбца вне допустимого диапазона\n");
        return ERR_OUT_RANGE;
    }

    return OK;
}

int input_array(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    printf("Input array element:\n");
    for (size_t i = 0; i < row; i++)
        for (size_t j = 0; j < col; j++)
        {
            printf("a[%ld][%ld]: ", i, j);
            if (scanf("%d", &a[i][j]) != 1)
            {
                printf("Error: Ввод/Вывод\n");
                return ERR;
            }
        }

    return OK;
}

void output_array(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    printf("Array element:\n");
    for (size_t i = 0; i < row; i++)
    {
        for (size_t j = 0; j < col; j++)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
}

int delete_row(int idx, int a[N_MAX][N_MAX], size_t *row, size_t *col)
{
    if (idx < 0)
        return ERR_OUT_RANGE;

    if (idx >= N_MAX)
        return ERR_OUT_RANGE;

    for (size_t i = idx; i < (*row) - 1; i++)
    {
        for (size_t j = 0; j < *col; j++)
        {
            a[i][j] = a[i + 1][j];
        }
    }

    // Устанавливаем последнюю строку в 0
    for (size_t j = 0; j < *col; j++)
    {
        a[(*row) - 1][j] = 0;
    }

    (*row) -= 1;

    return OK;
}

int delete_column(int idx, int a[N_MAX][N_MAX], size_t *row, size_t *col)
{
    if (idx < 0)
        return ERR_OUT_RANGE;

    if (idx >= N_MAX)
        return ERR_OUT_RANGE;

    for (size_t i = 0; i < *row; i++)
    {
        for (size_t j = idx; j < (*col) - 1; j++)
        {
            a[i][j] = a[i][j + 1];
        }
        a[i][(*col) - 1] = 0;
    }

    (*col) -= 1;

    return OK;
}