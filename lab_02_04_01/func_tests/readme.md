# Тесты для лабораторной работы № 02, Задание № 02

## Входные данные

целочисленные числа - элемента массива

## Выходные данные

целочисленные числа.

## Позитивные тесты

1. 01 - обычный тест
2. 02 - тест с 10 элементами
3. 03 - тест о братными порядками
4. 04 - тест с 10 элементами в обратным порядке

## Негативные тесты

1. 01 - первый элемент буква