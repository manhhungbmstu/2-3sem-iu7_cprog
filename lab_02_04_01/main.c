#include <stdio.h>
#include <stdlib.h>

#define OK 0
#define ERR 1
#define N 10

#define OK_100 100

int input_array(int *arr, size_t *size)
{
    printf("Input array element:\n");

    printf("arr[%d]: ", 0);
    if (scanf("%d", arr) != 1)
    {
        return ERR;
    }

    *size = 1;
    for (size_t i = 1; i <= N; i++)
    {
        printf("a[%zu]: ", i);
        if (scanf("%d", arr + i) != 1)
        {
            return OK;
        }
        (*size)++;
        if (*size > N)
        {
            *size = N;
            return OK_100;
        }
    }

    return OK;
}

int print_array(const int arr[], size_t size)
{
    for (size_t i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return OK;
}

void insertion_sort(int arr[], size_t size)
{
    int key, j;
    for (size_t i = 1; i < size; i++)
    {
        key = arr[i];
        j = i - 1;
        while (j >= 0 && arr[j] > key)
        {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = key;
    }
}

int main(void)
{
    int ret_code = OK;
    size_t size = 0;
    int arr[N + 1];

    ret_code = input_array(arr, &size);

    if (ret_code != OK && ret_code != OK_100)
    {
        printf("Ошибка: в массиве нет числа");
    }

    insertion_sort(arr, size);

    print_array(arr, size);

    return ret_code;
}
