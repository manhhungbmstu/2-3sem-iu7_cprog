#include <stdio.h>
#define N 10
#define OK 0
#define ERR -1
int input_array(int arr[], size_t size)
{
    printf("Введите элементов массива:\n");
    for (size_t i = 0; i < size; i++)
    {
        if (scanf("%d", &arr[i]) != 1)
        {
            printf("Ошибка: введено не число\n");
            return ERR;
        }
    }
    return OK;
}

int main()
{
    int arr[N];
    size_t size;
    int odd_product = 1;
    printf("Input size array 1-10: ");
    if (scanf("%zu", &size) != 1)
    {
        printf("Ошибка: введено не число\n");
        return ERR;
    }
    if (size == 0)
    {
        printf("Ошибка: размер массива равен 0\n");
        return ERR;
    }
    if (size > 10)
    {
        printf("Ошибка: размер массива неправильный");
        return ERR;
    }
    if (input_array(arr, size) != 0)
    {
        return ERR;
    }
    for (size_t i = 0; i < size; i++)
    {
        if (arr[i] % 2 != 0)
        {
            odd_product *= arr[i];
        }
    }

    if (odd_product == 1)
    {
        return ERR;
    }

    printf("Product: %d\n", odd_product);
    return OK;
}
