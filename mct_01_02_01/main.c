#include <stdio.h>
#include <stdbool.h>

#define OK 0
#define ERR_IO 1
#define ERR_RANGE 2
#define ERR_IDX_OUT 3

#define N_MAX 1024

int strerror(int err)
{
    switch (err)
    {
    case OK:
        break;
    case ERR_IO:
        printf("Error: Ввод/Вывод\n");
        break;
    case ERR_RANGE:
        printf("Error: Значение вне допустимого диапазона\n");
        break;
    case ERR_IDX_OUT:
        printf("Error: Индекс массив вне диапазона\n");
        break;
    default:
        printf("Error: Неизвестный код!\n");
        break;
    }

    return err;
}

int input_number_of_element(size_t *num)
{
    printf("Input number of element: ");
    if (scanf("%zu", num) != 1)
        return ERR_IO;
    if (*num <= 0 || *num > N_MAX)
        return ERR_RANGE;
    return OK;
}

int input_array(int *a, size_t n)
{
    printf("Input array element:\n");
    for (size_t i = 0; i < n; i++)
    {
        if (scanf("%d", a + i) != 1)
            return ERR_IO;
    }
    return OK;
}

void output_array(const int *a, size_t n)
{
    printf("Array element:\n");
    for (size_t i = 0; i < n; i++)
    {
        printf("%d ", a[i]);
    }
    printf("\n");
}

bool is_even(int num)
{
    if (num % 2 == 0)
        return true;
    else
        return false;
}

bool is_three_digit(int num)
{
    int n_digit = 0;
    while (num)
    {
        n_digit++;
        num /= 10;
    }
    if (n_digit == 3)
        return true;
    else
        return false;
}

int insert_array(int value, size_t idx, int *a, size_t *n)
{
    (*n) += 1;

    if (idx >= *n)
        return ERR_IDX_OUT;

    for (size_t i = *n - 1; i > idx; i--)
        a[i] = a[i - 1];
    a[idx] = value;

    return OK;
}

int delete_array(size_t idx, int *a, size_t *n)
{
    if (idx >= *n)
        return ERR_IDX_OUT;

    for (size_t i = idx; i < *n; i++)
        a[i] = a[i + 1];

    (*n) -= 1;
    return OK;
}

int filter_even(int *a, size_t *n)
{
    int ret = OK;
    size_t i = 0;
    while (i < *n)
    {
        if (!is_even(a[i]))
        {
            ret = delete_array(i, a, n);
            if (strerror(ret))
                return ret;
        }
        else
        {
            i++;
        }
    }
    return ret;
}

int filter_three_digit(int *a, size_t *n, int x)
{
    int ret = OK;
    size_t i = 0;
    while (i < *n)
    {
        if (is_three_digit(a[i]))
        {
            ret = insert_array(x, i + 1, a, n);
            if (strerror(ret))
                return ret;
            i++;
        }
        i++;
    }
    return ret;
}

int main(void)
{
    int ret = OK;
    size_t size = 0;
    // N_MAX*2 - мб все элементы четное и трехзначное
    int a[N_MAX * 2];
    int x;

    printf("Input x: ");
    if (scanf("%d", &x) != 1)
        ret = ERR_IO;
    if (strerror(ret))
        return ret;

    ret = input_number_of_element(&size);
    if (strerror(ret))
        return ret;

    ret = input_array(a, size);
    if (strerror(ret))
        return ret;

    ret = filter_even(a, &size);
    if (strerror(ret))
        return ret;

    ret = filter_three_digit(a, &size, x);
    if (strerror(ret))
        return ret;

    output_array(a, size);

    return ret;
}
