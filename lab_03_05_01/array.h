#ifndef ARRAY_H
#define ARRAY_H

#include <stdio.h>

#define OK 0
#define ERR 1
#define ERR_OUT_RANGE 2
#define ERR_NO_ELEMENT 3
#define ERR_UNDEF 4
#define N_MAX 10

int input_number_of_matrix(size_t *row, size_t *col);
int input_array(int a[N_MAX][N_MAX], size_t row, size_t col);
void output_array(int a[N_MAX][N_MAX], size_t row, size_t col);



#endif /* ARRAY_H */
