#include <stdio.h>
#include <stdlib.h>
#include "array.h"

int shift_matrix(int a[N_MAX][N_MAX], size_t row, size_t col);
int get_digit_sum(int n);

int main(void)
{
    size_t row = 0;
    size_t col = 0;
    int a[N_MAX][N_MAX];

    if (input_number_of_matrix(&row, &col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }

    if (input_array(a, row, col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }

    // проверка наличия элементов с суммой цифр больше 10
    int found = 0;
    for (size_t i = 0; i < row; i++)
    {
        for (size_t j = 0; j < col; j++)
        {
            if (get_digit_sum(a[i][j]) >= 10)
            {
                found = 1;
                break;
            }
        }
        if (found)
            break;
    }

    if (found == 0)
    {
        printf("Нет элемента с суммой > [10]\n");
        return ERR_NO_ELEMENT;
    }

    if (shift_matrix(a, row, col) == ERR_NO_ELEMENT)
    {
        printf("Нет элемента с суммой > [10]\n");
        return ERR_NO_ELEMENT;
    }

    output_array(a, row, col);

    return OK;
}

int get_digit_sum(int n)
{
    int sum = 0;
    n = abs(n);
    while (n > 0)
    {
        sum += abs(n % 10);
        n /= 10;
    }

    return sum;
}
int shift_matrix(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    const int threshold = 10;
    size_t count = 0;
    int elem[N_MAX * N_MAX];
    int found = 0;

    // искать элементы больше 10
    for (size_t i = 0; i < row; i++)
    {
        for (size_t j = 0; j < col; j++)
        {
            if (get_digit_sum(a[i][j]) > threshold)
            {
                elem[count] = a[i][j];
                count++;
                found = 1;
            }
        }
    }

    if (!found)
    {
        printf("Нет элемента с суммой > [10]\n");
        return ERR_NO_ELEMENT;
    }

    // сдвинуть на 3 позиции влево
    int tmp = elem[0];
    for (size_t i = 1; i < count; i++)
        elem[i - 1] = elem[i];
    elem[count - 1] = tmp;
    tmp = elem[0];
    for (size_t i = 1; i < count; i++)
        elem[i - 1] = elem[i];
    elem[count - 1] = tmp;
    tmp = elem[0];
    for (size_t i = 1; i < count; i++)
        elem[i - 1] = elem[i];
    elem[count - 1] = tmp;

    // вернуть элементы на место
    count = 0;
    for (size_t i = 0; i < row; i++)
    {
        for (size_t j = 0; j < col; j++)
        {
            if (get_digit_sum(a[i][j]) > threshold)
            {
                a[i][j] = elem[count];
                count++;
            }
        }
    }

    return OK;
}
