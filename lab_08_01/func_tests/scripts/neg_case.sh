#!/bin/bash
# Functional negative test program with argument
#

usage="usage: neg_case file_stream_in file_stream_out_expect file_args [-v]"

print_error() {
    echo "file_args: $file_args"
    echo "file_in: $file_in"
    echo "return_code: $ret_code"
    echo "file_out: $file_out != $file_expect"
}

# error: wrong parameter count
if [[ $# -lt 3 || $# -gt 4 ]]; then
    echo "neg_case: Wrong parameter count"
    echo "$usage"
    exit 2
fi

# error: wrong option
if [[ $# -eq 4 && $4 != "" && $4 != "-v" ]]; then
    echo "neg_case: Wrong option"
    echo "$usage"
    exit 3
fi

# filename
file_in=$1
file_expect=$2
file_args=$3
args=$(<"$file_args")
file_exe=./app.exe
file_out=/tmp/"$(basename "$file_expect")"

# not file
if [[ ! -f $file_in || ! -f $file_expect || ! -f $file_exe || ! -f $file_args ]]; then
    echo "neg_case: Missing file"
    echo "$usage"
    exit 4
fi

# run app
eval "$file_exe $args" <"$file_in" >/dev/null 2>"$file_out"
ret_code=$?
if [[ $ret_code -eq 0 ]]; then
    print_error
    exit 5
fi

# compare error message
# ../common/src/cmp_after_str.sh "$file_out" "$file_expect" "Error:" "$4"
# ret_code=$?
# if [[ $ret_code -ne 0 ]]; then
#     print_error
# fi
# exit $ret_code
exit 0
