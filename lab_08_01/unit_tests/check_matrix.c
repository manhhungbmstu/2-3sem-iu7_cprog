#include <stdio.h>
#include <stdlib.h>
#include <check.h>

#include "check_matrix.h"
#include "my_error.h"
#include "func_matrix.h"
// удалить строку с максимальными элементами
START_TEST(test_del_rows)
{

    int data1[3][2] = {{1, 2}, {3, 4}, {5, 6}};
    int data2[2][2] = {{1, 2}, {3, 4}};

    matrix test_matrix1;
    test_matrix1.r = 3;
    test_matrix1.c = 2;
    test_matrix1.matrix = malloc(test_matrix1.r * sizeof(int *));
    for (int i = 0; i < test_matrix1.r; i++)
    {
        test_matrix1.matrix[i] = malloc(test_matrix1.c * sizeof(int));
        for (int j = 0; j < test_matrix1.c; j++)
        {
            test_matrix1.matrix[i][j] = data1[i][j];
        }
    }

    matrix test_matrix2;
    test_matrix2.r = 2;
    test_matrix2.c = 2;
    test_matrix2.matrix = malloc(test_matrix2.r * sizeof(int *));
    for (int i = 0; i < test_matrix2.r; i++)
    {
        test_matrix2.matrix[i] = malloc(test_matrix2.c * sizeof(int));
        for (int j = 0; j < test_matrix2.c; j++)
        {
            test_matrix2.matrix[i][j] = data2[i][j];
        }
    }


    del_rows(&test_matrix1);
    del_rows(&test_matrix2);

    ck_assert_int_eq(test_matrix1.r, test_matrix1.c);
    ck_assert_int_eq(test_matrix2.r, test_matrix2.c);


    for (int i = 0; i < test_matrix1.r; i++)
    {
        free(test_matrix1.matrix[i]);
    }
    free(test_matrix1.matrix);

    for (int i = 0; i < test_matrix2.r; i++)
    {
        free(test_matrix2.matrix[i]);
    }
    free(test_matrix2.matrix);
}
END_TEST
START_TEST(test_del_rows_dublicate_max_element)
{

    int data1[3][2] = {{9, 2}, {9, 9}, {5, 6}};
    int data2[2][2] = {{9, 9}, {5, 6}};

    matrix test_matrix1;
    test_matrix1.r = 3;
    test_matrix1.c = 2;
    test_matrix1.matrix = malloc(test_matrix1.r * sizeof(int *));
    for (int i = 0; i < test_matrix1.r; i++)
    {
        test_matrix1.matrix[i] = malloc(test_matrix1.c * sizeof(int));
        for (int j = 0; j < test_matrix1.c; j++)
        {
            test_matrix1.matrix[i][j] = data1[i][j];
        }
    }

    matrix test_matrix2;
    test_matrix2.r = 2;
    test_matrix2.c = 2;
    test_matrix2.matrix = malloc(test_matrix2.r * sizeof(int *));
    for (int i = 0; i < test_matrix2.r; i++)
    {
        test_matrix2.matrix[i] = malloc(test_matrix2.c * sizeof(int));
        for (int j = 0; j < test_matrix2.c; j++)
        {
            test_matrix2.matrix[i][j] = data2[i][j];
        }
    }


    del_rows(&test_matrix1);
    del_rows(&test_matrix2);


    ck_assert_int_eq(test_matrix1.r, test_matrix1.c);
    ck_assert_int_eq(test_matrix2.r, test_matrix2.c);


    for (int i = 0; i < test_matrix1.r; i++)
    {
        free(test_matrix1.matrix[i]);
    }
    free(test_matrix1.matrix);

    for (int i = 0; i < test_matrix2.r; i++)
    {
        free(test_matrix2.matrix[i]);
    }
    free(test_matrix2.matrix);
}
END_TEST
// Удаление столбик
START_TEST(test_del_cols)
{

    int data[2][3] = {{1, 4, 2}, {2, 3, 1}};

    matrix test_matrix;
    test_matrix.r = 2;
    test_matrix.c = 3;
    test_matrix.matrix = malloc(test_matrix.r * sizeof(int *));
    for (int i = 0; i < test_matrix.r; i++)
    {
        test_matrix.matrix[i] = malloc(test_matrix.c * sizeof(int));
        for (int j = 0; j < test_matrix.c; j++)
        {
            test_matrix.matrix[i][j] = data[i][j];
        }
    }

    del_cols(&test_matrix);


    ck_assert_int_eq(test_matrix.c, 2);


    for (int i = 0; i < test_matrix.r; i++)
    {
        free(test_matrix.matrix[i]);
    }
    free(test_matrix.matrix);
}
END_TEST
// Найти среднее арифмитические значения
START_TEST(test_arithmetic_mean)
{
    matrix test_matrix;
    test_matrix.r = 3;
    test_matrix.c = 3;
    test_matrix.matrix = malloc(test_matrix.r * sizeof(int *));
    for (int i = 0; i < test_matrix.r; i++)
    {
        test_matrix.matrix[i] = malloc(test_matrix.c * sizeof(int));
        for (int j = 0; j < test_matrix.c; j++)
        {
            test_matrix.matrix[i][j] = i * test_matrix.c + j;
        }
    }

    int mean = arithmetic_mean(&test_matrix, 1);


    ck_assert_int_eq(mean, 2);


    for (int i = 0; i < test_matrix.r; i++)
    {
        free(test_matrix.matrix[i]);
    }
    free(test_matrix.matrix);
}
END_TEST

// добавление строки
START_TEST(test_add_row)
{
    matrix test_matrix;
    test_matrix.r = 2;
    test_matrix.c = 3;
    test_matrix.matrix = malloc(test_matrix.r * sizeof(int *));
    for (int i = 0; i < test_matrix.r; i++)
    {
        test_matrix.matrix[i] = malloc(test_matrix.c * sizeof(int));
        for (int j = 0; j < test_matrix.c; j++)
        {
            test_matrix.matrix[i][j] = i * test_matrix.c + j;
        }
    }

    int status = add_row(&test_matrix);


    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(test_matrix.r, 3);


    for (int i = 0; i < test_matrix.r; i++)
    {
        free(test_matrix.matrix[i]);
    }
    free(test_matrix.matrix);
}
END_TEST
START_TEST(test_max)
{
    int result1 = max(3, 7);
    int result2 = max(9, 4);


    ck_assert_int_eq(result1, 7);
    ck_assert_int_eq(result2, 9);
}
END_TEST

START_TEST(test_add_column)
{
    matrix test_matrix;
    test_matrix.r = 2;
    test_matrix.c = 2;
    test_matrix.matrix = malloc(test_matrix.r * sizeof(int *));
    for (int i = 0; i < test_matrix.r; i++)
    {
        test_matrix.matrix[i] = malloc(test_matrix.c * sizeof(int));
        for (int j = 0; j < test_matrix.c; j++)
        {
            test_matrix.matrix[i][j] = i * test_matrix.c + j;
        }
    }

    int status = add_column(&test_matrix);


    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(test_matrix.c, 3);


    for (int i = 0; i < test_matrix.r; i++)
    {
        free(test_matrix.matrix[i]);
    }
    free(test_matrix.matrix);
}
END_TEST
START_TEST(test_fill_in_to_become_squre)
{
    matrix a, b;

    a.r = 2;
    a.c = 3;
    a.matrix = malloc(a.r * sizeof(int *));
    for (int i = 0; i < a.r; i++)
    {
        a.matrix[i] = malloc(a.c * sizeof(int));
        for (int j = 0; j < a.c; j++)
        {
            a.matrix[i][j] = i * a.c + j;
        }
    }

    b.r = 3;
    b.c = 2;
    b.matrix = malloc(b.r * sizeof(int *));
    for (int i = 0; i < b.r; i++)
    {
        b.matrix[i] = malloc(b.c * sizeof(int));
        for (int j = 0; j < b.c; j++)
        {
            b.matrix[i][j] = i * b.c + j;
        }
    }

    int result = fill_in_to_become_squre(&a, &b);


    ck_assert_int_eq(result, 0);
    ck_assert_int_eq(a.r, b.r);


    for (int i = 0; i < a.r; i++)
    {
        free(a.matrix[i]);
    }
    free(a.matrix);

    for (int i = 0; i < b.r; i++)
    {
        free(b.matrix[i]);
    }
    free(b.matrix);
}
END_TEST

START_TEST(test_matrix_multiplication)
{
    matrix a, b, res;
    a.r = 2;
    a.c = 2;
    a.matrix = malloc(a.r * sizeof(int *));
    for (int i = 0; i < a.r; i++)
    {
        a.matrix[i] = malloc(a.c * sizeof(int));
        for (int j = 0; j < a.c; j++)
        {
            a.matrix[i][j] = i * a.c + j + 1;
        }
    }

    b.r = 2;
    b.c = 2;
    b.matrix = malloc(b.r * sizeof(int *));
    for (int i = 0; i < b.r; i++)
    {
        b.matrix[i] = malloc(b.c * sizeof(int));
        for (int j = 0; j < b.c; j++)
        {
            b.matrix[i][j] = i * b.c + j + 1;
        }
    }

    res.r = 2;
    res.c = 2;
    res.matrix = malloc(res.r * sizeof(int *));
    for (int i = 0; i < res.r; i++)
    {
        res.matrix[i] = malloc(res.c * sizeof(int));
    }

    matrix_multiplication(&res, &a, &b);


    ck_assert_int_eq(res.matrix[0][0], 7);
    ck_assert_int_eq(res.matrix[0][1], 10);
    ck_assert_int_eq(res.matrix[1][0], 15);
    ck_assert_int_eq(res.matrix[1][1], 22);


    for (int i = 0; i < a.r; i++)
    {
        free(a.matrix[i]);
    }
    free(a.matrix);

    for (int i = 0; i < b.r; i++)
    {
        free(b.matrix[i]);
    }
    free(b.matrix);

    for (int i = 0; i < res.r; i++)
    {
        free(res.matrix[i]);
    }
    free(res.matrix);
}
END_TEST
Suite *matrix_suite(void)
{
    Suite *suite = suite_create("matrix");

    TCase *tc_r_row = tcase_create("Delrows");
    TCase *tc_r_col = tcase_create("Delcols");
    TCase *tc_ar_mean = tcase_create("Arithmeticmean");
    TCase *tc_add_row = tcase_create("Addrow");
    TCase *tc_square = tcase_create("FillInToBecomeSquare");
    TCase *tc_add_col = tcase_create("Addcol");
    TCase *tc_max = tcase_create("Findmax");

    TCase *tc_mul = tcase_create("Multiplication");

    tcase_add_test(tc_r_row, test_del_rows);
    tcase_add_test(tc_r_row, test_del_rows_dublicate_max_element);
    tcase_add_test(tc_r_col, test_del_cols);
    tcase_add_test(tc_ar_mean, test_arithmetic_mean);
    tcase_add_test(tc_add_row, test_add_row);
    tcase_add_test(tc_square, test_fill_in_to_become_squre);
    tcase_add_test(tc_add_col, test_add_column);
    tcase_add_test(tc_max, test_max);

    tcase_add_test(tc_mul, test_matrix_multiplication);

    suite_add_tcase(suite, tc_r_row);
    suite_add_tcase(suite, tc_r_col);
    suite_add_tcase(suite, tc_ar_mean);
    suite_add_tcase(suite, tc_add_row);
    suite_add_tcase(suite,tc_square);
    suite_add_tcase(suite,tc_add_col);
    suite_add_tcase(suite,tc_max);


    suite_add_tcase(suite,tc_mul);
 
    return suite;
}
