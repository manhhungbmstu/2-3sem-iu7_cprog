#include "func_IO.h"
#include "func_matrix.h"
#include "my_error.h"

int main(void)
{   
    matrix a, b;

    int rc = read_matrices(&a, &b);

    if (rc)
    {
        message_error(rc);
        return rc;
    }

    // printf("Matrix A before reduction:\n");
    // print_matrix(&a);

    // printf("\nMatrix B before reduction:\n");
    // print_matrix(&b);

    reduce_matrix(&a);
    reduce_matrix(&b);

    // printf("\nMatrix A after reduction:\n");
    // print_matrix(&a);

    // printf("\nMatrix B after reduction:\n");
    // print_matrix(&b);

    rc = fill_in_to_become_squre(&a, &b);

    if (rc)
    {
        message_error(rc);
        return rc;
    }

    // printf("\nMatrix A after filling in to become square:\n");
    // print_matrix(&a);

    // printf("\nMatrix B after filling in to become square:\n");
    // print_matrix(&b);

    rc = matrix_exponentiation(&a, &b);

    if (rc)
    {
        message_error(rc);
        return rc;
    }

    matrix res;

    rc = get_result(&res, &a, &b);

    if (rc)
    {
        message_error(rc);
        return rc;
    }

    // printf("\nResult matrix after matrix exponentiation:\n");
    print_matrix(&res);

    free_mem(a.matrix, a.r);
    free_mem(b.matrix, b.r);
    free_mem(res.matrix, res.r);

    return rc;
}
