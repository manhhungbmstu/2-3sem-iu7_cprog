#ifndef _FUNC_IO_H_
#define _FUNC_IO_H_

#include <stdio.h>
#include <stdlib.h>

#include "my_error.h"
/**
 * @brief ввод размер для матрицы
 *
 */
int input_size_matrix(int *row, int *col);
/**
 * @brief ввод степени
 *
 */
int input_power(int *alpha, int *beta);
/**
 * @brief ввод элемнты для матрицы
 *
 */
int input_matrix(matrix *a);
/**
 * @brief выделение память для матрицы
 *
 */
int **allocate_matrix(int n, int m);
void free_mem(int **data, int n);

int read_matrices(matrix *a, matrix *b);
/**
 * @brief выводить матрицы на экране
 *
 */
void print_matrix(matrix *a);

#endif
