#include "array.h"

int input_number_of_matrix(size_t *row, size_t *col)
{
    printf("Введите номер строки: ");
    if (scanf("%zu", row) != 1)
    {
        printf("Error: Ввод/Вывод\n");
        return ERR;
    }
    if (*row <= 0 || *row > N_MAX)
    {
        printf("Ошибка: значение строка вне допустимого диапазона\n");
        return ERR_OUT_RANGE;
    }

    printf("Введите номер столбики: ");
    if (scanf("%zu", col) != 1)
    {
        printf("Error: Ввод/Вывод\n");
        return ERR;
    }
    if (*col <= 0 || *col > N_MAX)
    {
        printf("Ошибка: значение столбца вне допустимого диапазона\n");
        return ERR_OUT_RANGE;
    }

    return OK;
}

int input_array(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    printf("Input array element:\n");
    for (size_t i = 0; i < row; i++)
        for (size_t j = 0; j < col; j++)
        {
            printf("a[%ld][%ld]: ", i, j);
            if (scanf("%d", &a[i][j]) != 1)
            {
                printf("Error: Ввод/Вывод\n");
                return ERR;
            }
        }

    return OK;
}

void output_array(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    printf("Array element:\n");
    for (size_t i = 0; i < row; i++)
    {
        for (size_t j = 0; j < col; j++)
        {
            printf("%d ", a[i][j]);
        }
        printf("\n");
    }
}

int sort_row_desc(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    for (size_t i = 0; i < row; i++)
    {
        size_t largest_idx = i;
        for (size_t j = i + 1; j < row; j++)
        {
            int largest_elem = a[j][0];
            for (size_t k = 1; k < col; k++)
            {
                if (a[j][k] > largest_elem)
                {
                    largest_elem = a[j][k];
                }
            }
            if (largest_elem > a[largest_idx][0])
            {
                largest_idx = j;
            }
        }
        if (largest_idx != i)
        {
            for (size_t k = 0; k < col; k++)
            {
                int temp = a[i][k];
                a[i][k] = a[largest_idx][k];
                a[largest_idx][k] = temp;
            }
        }
    }
    return OK;
}
