#include "../inc/my_function.h"

void inputElements(struct Node **head_ref, int num_elements)
{
    for (int i = 0; i < num_elements; i++)
    {
        int new_data;
        printf("Enter element %d: ", i + 1);

        if (scanf("%d", &new_data) != 1)
        {
            printf("ввод число\n");
            while (getchar() != '\n');
            i--;
            continue;
        }

        insertAtEnd(head_ref, new_data);
    }
}

void insertAtEnd(struct Node **head_ref, int new_data)
{
    struct Node *new_node = (struct Node *)malloc(sizeof(struct Node));
    struct Node *last = *head_ref;
    new_node->data = new_data;
    new_node->next = NULL;
    if (*head_ref == NULL)
    {
        *head_ref = new_node;
        return;
    }
    while (last->next != NULL)
    {
        last = last->next;
    }
    last->next = new_node;
}

int findsecondmax(struct Node *head)
{
    // добавление проверку когда пустая
    if (head == NULL)
    {
        return INT_MIN;
    }
    int max1 = head->data;
    int max2 = INT_MIN;

    while (head != NULL)
    {
        if (head->data > max1)
        {
            max2 = max1;
            max1 = head->data;
        }
        else if (head->data > max2 && head->data < max1)
        {
            max2 = head->data;
        }
        head = head->next;
    }

    return max2;
}


void deleteNodeWithValue(struct Node **head_ref, int value)
{
    struct Node *current = *head_ref;
    struct Node *prev = NULL;

    while (current != NULL && current->data != value)
    {
        prev = current;
        current = current->next;
    }

    if (current != NULL)
    {

        if (prev == NULL)
        {
            *head_ref = current->next;
        }
        else
        {
            prev->next = current->next;
        }
        free(current);
    }
}
void printList(struct Node *node)
{
    while (node != NULL)
    {
        printf("%d ", node->data);
        node = node->next;
    }
    printf("\n");
}
void freeList(struct Node **head_ref)
{
    struct Node *current = *head_ref;
    struct Node *next;

    while (current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }

    *head_ref = NULL;
}

int input_num_elements()
{
    int n;
    printf("Enter the number of elements: ");
    if (scanf("%d", &n) != 1)
    {
        printf("error input\n");
        return ERR_IO;
    }
    return n;
}
