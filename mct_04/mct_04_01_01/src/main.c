#include <stdio.h>
#include <stdlib.h>
#include "../inc/my_function.h"

int main()
{
    struct Node *head = NULL;
    int num_elements;

    num_elements = input_num_elements();

    if (num_elements == -1 || num_elements < 0)
    {
        printf("error input\n");
        return ERR_IO; 
    }
    if (num_elements == 0)
    {
        printf("empty\n");
        return ERR_EMPTY;
    }
    inputElements(&head, num_elements);

    printf("список сначала: \n");
    printList(head);

    int secondmax = findsecondmax(head);

    deleteNodeWithValue(&head, secondmax);

    printf("список после удалении: \n");
    printList(head);

    freeList(&head);

    return 0;
}
