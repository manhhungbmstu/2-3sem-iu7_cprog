#include <stdio.h>
#include <stdlib.h>

#include "../inc/check_myfunction.h"
#include "../inc/my_function.h"
// region findsecondmax
START_TEST(test_findsecondmax_multiple_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, 1);
    insertAtEnd(&head, 2);
    insertAtEnd(&head, 3);
    int result = findsecondmax(head);

    ck_assert_msg(result == 2, "Ожидаемый второй максимум будет 2");

    freeList(&head);
}
END_TEST
START_TEST(test_findsecondmax_single_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, 1);

    int result = findsecondmax(head);

    ck_assert_msg(result == INT_MIN, "Ожидаемый второй максимум будет INT_MIN");

    freeList(&head);
}
END_TEST
START_TEST(test_findsecondmax_with_negative_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, -1);
    insertAtEnd(&head, -2);
    insertAtEnd(&head, -3);
    int result = findsecondmax(head);

    ck_assert_msg(result == -2, "Ожидаемый второй максимум будет -2");

    freeList(&head);
}
END_TEST
START_TEST(test_findsecondmax_empty_list)
{

    struct Node *head = NULL;

    int result = findsecondmax(head);

    ck_assert_msg(result == INT_MIN, "Ожидаемый второй максимум будет INT_MIN");

    freeList(&head);
}
END_TEST
// endregion

START_TEST(test_deleteNodeWithValue_multiple_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, 1);
    insertAtEnd(&head, 2);
    insertAtEnd(&head, 3);

    int value_to_delete = 2;
    deleteNodeWithValue(&head, value_to_delete);

    ck_assert_msg(head->data == 1, "Ожидаемое значение головы равно 1.");
    ck_assert_msg(head->next->data == 3, "Ожидаемое значение второго элемента равно 3.");
    ck_assert_msg(head->next->next == NULL, "Ожидаемый третий элемент будет NULL");

    freeList(&head);
}
END_TEST
START_TEST(test_deleteSecondMaxNode_single_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, 1);

    deleteNodeWithValue(&head, 1);

    ck_assert_msg(head == NULL, "Ожидаемое значение головы равно NULL");

    freeList(&head);
}
END_TEST
START_TEST(test_deleteNodeWithValue_empty_list)
{
    struct Node *head = NULL;

    deleteNodeWithValue(&head, 1);

    ck_assert_msg(head == NULL, "Ожидаемое значение головы равно NULL");

    freeList(&head);
}
END_TEST
START_TEST(test_deleteNodeWithValue_with_negative_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, -1);
    insertAtEnd(&head, -2);
    insertAtEnd(&head, -3);

    int value_to_delete = -2;
    deleteNodeWithValue(&head, value_to_delete);

    ck_assert_msg(head->data == -1, "Ожидаемое значение головы равно -1.");
    ck_assert_msg(head->next->data == -3, "Ожидаемое значение второго элемента равно -3.");
    ck_assert_msg(head->next->next == NULL, "Ожидаемый третий элемент будет NULL");

    freeList(&head);
}
END_TEST
// endregion
Suite *myfunction_suite(void)
{
    Suite *suite = suite_create("my_function");

    TCase *tc_pos = tcase_create("positives");
    tcase_add_test(tc_pos, test_findsecondmax_multiple_element);
    tcase_add_test(tc_pos, test_findsecondmax_single_element);
    tcase_add_test(tc_pos, test_findsecondmax_with_negative_element);
    tcase_add_test(tc_pos, test_findsecondmax_empty_list);

    tcase_add_test(tc_pos, test_deleteNodeWithValue_multiple_element);
    tcase_add_test(tc_pos, test_deleteSecondMaxNode_single_element);
    tcase_add_test(tc_pos, test_deleteNodeWithValue_empty_list);
    tcase_add_test(tc_pos, test_deleteNodeWithValue_with_negative_element);

    suite_add_tcase(suite, tc_pos);
    return suite;
}
