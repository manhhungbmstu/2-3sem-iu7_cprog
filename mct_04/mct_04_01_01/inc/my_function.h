#ifndef MYFUNCTION_H
#define MYFUNCTION_H
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#define OK 0
#define ERR_IO -1
#define ERR_EMPTY -2

struct Node
{
    int data;
    struct Node *next;
};

void insertAtEnd(struct Node **head_ref, int new_data);
void inputElements(struct Node **head_ref, int num_elements);
int findsecondmax(struct Node *head);
void deleteNodeWithValue(struct Node **head_ref, int value);
void printList(struct Node *node);
void freeList(struct Node **head_ref);
int input_num_elements();
#endif