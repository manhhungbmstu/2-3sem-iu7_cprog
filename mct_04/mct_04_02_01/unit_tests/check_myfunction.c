#include <stdio.h>
#include <stdlib.h>

#include "../inc/check_myfunction.h"
#include "../inc/my_function.h"
// region findmin
START_TEST(test_findmin_multiple_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, 1);
    insertAtEnd(&head, 2);
    insertAtEnd(&head, 3);
    struct Node *result = findmin(head);

    ck_assert_msg(result->data == 1, "Ожидаемый минимум должен быть 1");

    freeList(&head);
}
END_TEST
START_TEST(test_findmin_single_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, 5); 
    struct Node *result = findmin(head);

    ck_assert_msg(result->data == 5, "Ожидаемый минимум должен быть 5");

    freeList(&head);
}
END_TEST
START_TEST(test_findmin_with_negative_element)
{
    struct Node *head = NULL;
    insertAtEnd(&head, -1);
    insertAtEnd(&head, -2);
    insertAtEnd(&head, -3);
    struct Node *result = findmin(head);

    ck_assert_msg(result->data == -3, "Ожидаемый минимум должен быть -3");

    freeList(&head);
}
END_TEST
START_TEST(test_findmin_empty_list)
{
    struct Node *head = NULL;

    struct Node *result = findmin(head);

    ck_assert_msg(result == NULL, "Ожидаем, что результат будет NULL для пустого списка");

    freeList(&head);
}
END_TEST

// endregion
// region swap
START_TEST(test_swap_min_and_last)
{
    struct Node *head = NULL;
    insertAtEnd(&head, 1);
    insertAtEnd(&head, 2);
    insertAtEnd(&head, 3);

    struct Node *minNode = findmin(head);

    struct Node *lastNode = head;
    while (lastNode->next != NULL)
    {
        lastNode = lastNode->next;
    }

    ck_assert_msg(minNode->data != lastNode->data, "Ожидаем, что значения min и last различны");

    swap(minNode, lastNode);
    ck_assert_msg(minNode->data == 3 && lastNode->data == 1, "Ожидаем, что значения min и last поменялись");

    freeList(&head);
}
END_TEST
START_TEST(test_swap_min_and_last_with_duplicates)
{
    struct Node *head = NULL;
    insertAtEnd(&head, -3);
    insertAtEnd(&head, -2);
    insertAtEnd(&head, -3);
    insertAtEnd(&head, -1);
    insertAtEnd(&head, -3);

    struct Node *minNode = findmin(head);

    struct Node *lastNode = head;
    while (lastNode->next != NULL)
    {
        lastNode = lastNode->next;
    }

    swap(minNode, lastNode);

    ck_assert_msg(minNode->data == -3 && lastNode->data == -3, "Ожидаем, что значения min и last поменялись");

    freeList(&head);
}
END_TEST

START_TEST(test_swap_min_and_last_empty_list)
{
    struct Node *head = NULL;

    struct Node *minNode = findmin(head);

    struct Node *lastNode = head;

    swap(minNode, lastNode);

    ck_assert_msg(minNode == NULL && lastNode == NULL, "Ожидаем, что значения min и last не изменились в пустом списке");

    freeList(&head);
}
END_TEST

// endregion
Suite *myfunction_suite(void)
{
    Suite *suite = suite_create("my_function");

    TCase *tc_pos = tcase_create("positives");
    tcase_add_test(tc_pos, test_findmin_multiple_element);
    tcase_add_test(tc_pos, test_findmin_single_element);
    tcase_add_test(tc_pos, test_findmin_with_negative_element);
    tcase_add_test(tc_pos, test_findmin_empty_list);

    tcase_add_test(tc_pos, test_swap_min_and_last);
    tcase_add_test(tc_pos, test_swap_min_and_last_with_duplicates);
    tcase_add_test(tc_pos, test_swap_min_and_last_empty_list);
    suite_add_tcase(suite, tc_pos);
    return suite;
}
