#include "../inc/my_function.h"

void insertAtEnd(struct Node **head_ref, int new_data)
{
    struct Node *new_node = (struct Node *)malloc(sizeof(struct Node));
    struct Node *last = *head_ref;
    new_node->data = new_data;
    new_node->next = NULL;
    if (*head_ref == NULL)
    {
        *head_ref = new_node;
        return;
    }
    while (last->next != NULL)
    {
        last = last->next;
    }
    last->next = new_node;
}

struct Node *findmin(struct Node *head)
{
    struct Node *minNode = NULL;
    int minVal = INT_MAX;

    while (head != NULL)
    {
        if (head->data < minVal)
        {
            minVal = head->data;
            minNode = head;
        }
        head = head->next;
    }

    return minNode;
}

void swap(struct Node *node1, struct Node *node2)
{
    if (node1 == NULL || node2 == NULL)
    {
        return;
    }

    int temp = node1->data;
    node1->data = node2->data;
    node2->data = temp;
}

void printList(struct Node *node)
{
    while (node != NULL)
    {
        printf("%d ", node->data);
        node = node->next;
    }
    printf("\n");
}
void freeList(struct Node **head_ref)
{
    struct Node *current = *head_ref;
    struct Node *next;

    while (current != NULL)
    {
        next = current->next;
        free(current);
        current = next;
    }

    *head_ref = NULL;
}
int input_num_elements()
{
    int n;
    printf("Enter the number of elements: ");
    if (scanf("%d", &n) != 1)
    {
        printf("error input\n");
        return ERR_IO;
    }
    return n;
}
void inputElements(struct Node **head_ref, int num_elements)
{
    for (int i = 0; i < num_elements; i++)
    {
        int new_data;
        printf("Enter element %d: ", i + 1);

        if (scanf("%d", &new_data) != 1)
        {
            printf("ввод число\n");
            while (getchar() != '\n')
                ;
            i--;
            continue;
        }

        insertAtEnd(head_ref, new_data);
    }
}