#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "../inc/my_function.h"
// поменять из списка втрой максимум и последний элемент
int main()
{
    struct Node *head = NULL;
    int num_elements;

    num_elements = input_num_elements();

    if (num_elements == -1 || num_elements < 0)
    {
        printf("error input\n");
        return ERR_IO; 
    }
    if (num_elements == 0)
    {
        printf("empty\n");
        return ERR_EMPTY;
    }
    inputElements(&head, num_elements);

    printf("список сначала: ");
    printList(head);

    struct Node *minNode = findmin(head);

    struct Node *lastNode = head;
    while (lastNode->next != NULL)
    {
        lastNode = lastNode->next;
    }

    if (minNode != NULL && lastNode != NULL)
    {
        swap(minNode, lastNode);
    }

    printf("список после изменения: ");
    printList(head);
    freeList(&head);
    return 0;
}

