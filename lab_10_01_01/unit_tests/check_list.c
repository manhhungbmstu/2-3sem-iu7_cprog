#include <stdio.h>
#include <stdlib.h>

#include "check_list.h"
#include "list.h"

node_t *array2list(int *arr, size_t size)
{
    node_t *head = NULL;
    for (size_t i = size; i > 0; i--)
    {
        node_t *new_node = list_new(arr + i - 1);
        if (new_node)
        {
            new_node->next = head;
            head = new_node;
        }
        else
        {
            list_free(head);
            head = NULL;
            break;
        }
    }
    return head;
}

// region front_back_split
START_TEST(test_front_back_split)
{

    node_t *list = NULL;

    int new_data = 1;
    node_t *new_elem = list_new(&new_data);
    insert(&list, new_elem, NULL);

    ck_assert_int_eq(*(int *)new_elem->data, new_data);

    list_free(list);
}
END_TEST

// Пустой список
START_TEST(test_front_back_split_empty_list)
{

    node_t *list = NULL;

    node_t *back = NULL;

    front_back_split(list, &back);

    ck_assert_ptr_null(list);

    ck_assert_ptr_null(back);
}
END_TEST

// endregion


// region sort

// Пустой список
START_TEST(test_sort_empty_list)
{
    node_t *list = NULL;
    node_t *sorted_list = sort(list, int_cmp);

    ck_assert_ptr_null(sorted_list);

    list_free(sorted_list);
}
END_TEST

// Один элемент
START_TEST(test_sort_single_elem)
{
    int arr[] = {1};
    node_t *list = array2list(arr, sizeof(arr) / sizeof(arr[0]));

    node_t *sorted_list = sort(list, int_cmp);

    int rc = list_is_equal(sorted_list, list, int_cmp);
    ck_assert_int_eq(rc, 1);

    list_free(sorted_list);
}
END_TEST

// Тест сортировки списка с несколькими элементами
START_TEST(test_sort_multiple_elements)
{
    int arr[] = {4, 2, 1, 3, 5};
    node_t *list = array2list(arr, sizeof(arr) / sizeof(arr[0]));

    node_t *sorted_list = sort(list, int_cmp);

    int arr_exp[] = {1, 2, 3, 4, 5};
    node_t *expected_list = array2list(arr_exp, sizeof(arr_exp) / sizeof(arr_exp[0]));

    int rc = list_is_equal(sorted_list, expected_list, int_cmp);
    ck_assert_int_eq(rc, 1);

    list_free(sorted_list);
    list_free(expected_list);
}
END_TEST

// Тест сортировки списка с повторяющимися элементами
START_TEST(test_sort_with_duplicates)
{
    int arr[] = {4, 2, 1, 3, 5, 2, 4};
    node_t *list = array2list(arr, sizeof(arr) / sizeof(arr[0]));

    node_t *sorted_list = sort(list, int_cmp);

    int arr_exp[] = {1, 2, 2, 3, 4, 4, 5};
    node_t *expected_list = array2list(arr_exp, sizeof(arr_exp) / sizeof(arr_exp[0]));

    int rc = list_is_equal(sorted_list, expected_list, int_cmp);
    ck_assert_int_eq(rc, 1);

    list_free(sorted_list);
    list_free(expected_list);
}
END_TEST

// endregion

// region find

// Нет элемент
START_TEST(test_find_not_found)
{

    node_t *head = NULL;
    int searchValue = 6;
    node_t *result = find(head, &searchValue, int_cmp);

    ck_assert_ptr_null(result);
}
END_TEST

// есть элемент
START_TEST(test_find_found)
{
    node_t *head = NULL;

    int values[] = {1, 2, 3, 4, 5};
    for (size_t i = 0; i < sizeof(values) / sizeof(values[0]); ++i)
    {
        node_t *newNode = (node_t *)malloc(sizeof(node_t));
        newNode->data = malloc(sizeof(int));
        *(int *)(newNode->data) = values[i];
        newNode->next = head;
        head = newNode;
    }

    int searchValue = 3;
    node_t *result = find(head, &searchValue, int_cmp);

    ck_assert_ptr_nonnull(result);
    ck_assert_int_eq(*(int *)(result->data), searchValue);

    while (head != NULL)
    {
        node_t *temp = head;
        head = head->next;
        free(temp->data);
        free(temp);
    }
}
END_TEST

// endregion

// region insert

// В начале
START_TEST(test_insert_in_begin)
{

    int initial_data = 1;
    node_t *list = list_new(&initial_data);

    int new_data = 0;
    node_t *new_elem = list_new(&new_data);
    insert(&list, new_elem, list);

    ck_assert_int_eq(*(int *)list->data, new_data);

    // Очищаем память
    list_free(list);
}
END_TEST

// В конце
START_TEST(test_insert_in_end)
{

    int initial_data = 1;
    node_t *list = list_new(&initial_data);

    int new_data = 2;
    node_t *new_elem = list_new(&new_data);
    insert(&list, new_elem, NULL);

    node_t *current = list;
    while (current->next != NULL)
    {
        current = current->next;
    }

    ck_assert_int_eq(*(int *)new_elem->data, new_data);

    // Очищаем память
    list_free(list);
    list_free(new_elem);
}
END_TEST

// endregion

// region append
// добавить элементы
START_TEST(test_append)
{
    int arr_a[] = {1, 2, 3};
    int arr_b[] = {4, 5, 6};

    // Создаем списки
    node_t *list_a = array2list(arr_a, sizeof(arr_a) / sizeof(arr_a[0]));
    node_t *list_b = array2list(arr_b, sizeof(arr_b) / sizeof(arr_b[0]));

    append(&list_a, &list_b);

    ck_assert(list_a != NULL);
    ck_assert_int_eq(*(int *)list_a->data, 1);

    node_t *current = list_a;
    while (current->next != NULL)
    {
        current = current->next;
    }

    if (current->next != NULL)
    {
        ck_assert_int_eq(*(int *)current->next->data, 4);
    }

    list_free(list_a);
}
END_TEST

// список b пустой
START_TEST(test_append_empty_list_b)
{
    int arr_a[] = {1, 2, 3};

    node_t *list_a = array2list(arr_a, sizeof(arr_a) / sizeof(arr_a[0]));
    node_t *list_b = NULL;

    append(&list_a, &list_b);

    ck_assert(list_a != NULL);
    ck_assert_int_eq(*(int *)list_a->data, 1);

    ck_assert_ptr_eq(list_b, NULL);

    list_free(list_a);
}
END_TEST
// оба пустые
START_TEST(test_append_both_empty_lists)
{
    node_t *list_a = NULL;
    node_t *list_b = NULL;

    append(&list_a, &list_b);

    ck_assert_ptr_null(list_a);

    ck_assert_ptr_null(list_b);
}
END_TEST

// endregion

Suite *list_suite(void)
{
    Suite *suite = suite_create("list");

    TCase *tc_pos = tcase_create("positives");
    tcase_add_test(tc_pos, test_front_back_split);
    tcase_add_test(tc_pos, test_front_back_split_empty_list);

    tcase_add_test(tc_pos, test_sort_multiple_elements);
    tcase_add_test(tc_pos, test_sort_empty_list);
    tcase_add_test(tc_pos, test_sort_single_elem);
    tcase_add_test(tc_pos, test_sort_with_duplicates);

    tcase_add_test(tc_pos, test_find_not_found);
    tcase_add_test(tc_pos, test_find_found);

    tcase_add_test(tc_pos, test_insert_in_begin);
    tcase_add_test(tc_pos, test_insert_in_end);

    tcase_add_test(tc_pos, test_append);
    tcase_add_test(tc_pos, test_append_empty_list_b);
    tcase_add_test(tc_pos, test_append_both_empty_lists);

    suite_add_tcase(suite, tc_pos);
    return suite;
}
