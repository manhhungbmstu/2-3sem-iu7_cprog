# Компилятор и флаги
CC := gcc
CFLAGS := -Iinc -std=c99 -Wall -Werror -Wextra -Wpedantic -Wfloat-equal -Wfloat-conversion -Wvla -g -fprofile-arcs -ftest-coverage
CFLAGS_RELEASE := $(CFLAGS) -O2
LDFLAGS := -fprofile-arcs -ftest-coverage -lcheck -lpthread -lrt -lm -lgcov --coverage
ifeq ($(shell grep -o "^ID=.*" /etc/os-release | cut -d'=' -f2), ubuntu)
	LDFLAGS += -lsubunit
endif
LDFLAGS_RELEASE := -lm

# Директории
SRC_DIR := src
INC_DIR := inc
TEST_DIR := unit_tests
OUT_DIR := out

# Файлы
APP_EXEC := app.exe
UNIT_TEST_EXEC := unit_tests.exe

# Исходные файлы и объектные файлы
APP_SRC := $(wildcard $(SRC_DIR)/*.c)
APP_OBJ := $(APP_SRC:$(SRC_DIR)/%.c=$(OUT_DIR)/%.o)

UNIT_TEST_SRC := $(wildcard $(TEST_DIR)/check_*.c)
UNIT_TEST_OBJ := $(UNIT_TEST_SRC:$(TEST_DIR)/%.c=$(OUT_DIR)/%.o)

# Цели
.PHONY: all clean unit func cov release debug re

# Основные цели
all:  $(UNIT_TEST_EXEC)

$(APP_EXEC): $(APP_OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

$(UNIT_TEST_EXEC): $(UNIT_TEST_OBJ) $(filter-out $(OUT_DIR)/main.o, $(APP_OBJ))
	$(CC) -o $@ $^ $(LDFLAGS)

# Правило для объектных файлов
$(OUT_DIR)/%.o: $(SRC_DIR)/%.c | $(OUT_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(OUT_DIR)/%.o: $(TEST_DIR)/%.c | $(OUT_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

$(OUT_DIR):
	mkdir -p $(OUT_DIR)


unit: $(UNIT_TEST_EXEC)
	export CK_FORK=no && valgrind --leak-check=full ./unit_tests.exe

func: $(APP_EXEC)
	./func_tests/scripts/func_tests.sh

cov:
	cd ./out && gcov -o . ../src/*.c && cd ..

release: CFLAGS := $(CFLAGS_RELEASE)
release: LDFLAGS := $(LDFLAGS_RELEASE)
release: $(APP_EXEC)

debug: $(APP_EXEC)

re: clean all

clean:
	rm -rf $(OUT_DIR) $(APP_EXEC) $(UNIT_TEST_EXEC)
