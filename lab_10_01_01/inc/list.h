#ifndef LIST_H__
#define LIST_H__

#include "callback.h"

typedef struct node node_t;

struct node
{
    void *data;
    node_t *next;
};
int int_cmp(const void *a, const void *b);
/**
 * @brief Поиск элемента в списке
 */
node_t *find(node_t *head, const void *data, int (*cmp)(const void *pl, const void *pr));

/**
 * @brief Вставляет элемент перед указанным элементом списка
 */
void insert(node_t **head, node_t *elem, node_t *before);

/**
 * @brief добавляет список
 */
void append(node_t **head_a, node_t **head_b);

/**
 * @brief Сортировать список
 */

node_t *sort(node_t *head, compare_func_t cmp);

/**
 * @brief Делит списка на две половины
 */
void front_back_split(node_t *head, node_t **back);

/**
 * @brief Объединяет два упорядоченного списка в одни упорядоченный
 */
node_t *sorted_merge(node_t **head_a, node_t **head_b, compare_func_t cmp);

// MY

/**
 * @brief Создание нового элемента
 */
node_t *list_new(void *data);

/**
 * @brief Освобождение памяти элемент списка, без освобождение `data`
 */
void list_free(node_t *head);

/**
 * @brief Два списка равны?
 */
int list_is_equal(const node_t *list_a, const node_t *list_b, compare_func_t cmp);

#endif
