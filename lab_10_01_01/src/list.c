#include <stdlib.h>
#include <assert.h>

#include "list.h"

// region Задачи на работу с одним элементом списка
int int_cmp(const void *a, const void *b)
{
    return (*(int *)a - *(int *)b);
}

node_t *find(node_t *head, const void *data, int (*compare_func_t)(const void *pl, const void *pr))
{
    node_t *current = head;

    while (current != NULL)
    {
        if (compare_func_t(current->data, data) == 0)
        {
            return current;
        }

        // Переходим к следующему узлу
        current = current->next;
    }

    // Элемент не найден
    return NULL;
}

void insert(node_t **head, node_t *elem, node_t *before)
{
    if (!elem || !head)
        return;

    if (before == *head)
    {
        elem->next = *head;
        *head = elem;
        return;
    }

    node_t *curr = *head;
    while (curr && curr->next != before)
        curr = curr->next;

    if (curr && before)
    {
        elem->next = before;
        curr->next = elem;
    }
    else if (!curr && !before)
    {
        elem->next = NULL;
        if (!*head)
            *head = elem;
        else
        {
            curr = *head;
            while (curr->next)
                curr = curr->next;
            curr->next = elem;
        }
    }
}
// endregion

// region Задачи на работу с целым списком

void append(node_t **head_a, node_t **head_b)
{
    if (*head_a == NULL)
    {
        *head_a = *head_b;
    }
    else
    {

        node_t *current = *head_a;
        while (current->next != NULL)
        {
            current = current->next;
        }

        current->next = *head_b;
    }

    *head_b = NULL;
}

// endregion

// region Сортировка списка

node_t *sort(node_t *head, compare_func_t cmp)
{
    if (!head || !head->next)
        return head;

    node_t *back;
    front_back_split(head, &back);

    head = sort(head, cmp);
    back = sort(back, cmp);

    return sorted_merge(&head, &back, cmp);
}

void front_back_split(node_t *head, node_t **back)
{
    if (head == NULL || head->next == NULL)
    {
        *back = NULL;
        return;
    }

    node_t *slow = head;
    node_t *fast = head->next;

    while (fast != NULL)
    {
        fast = fast->next;
        if (fast != NULL)
        {
            slow = slow->next;
            fast = fast->next;
        }
    }

    *back = slow->next;
    slow->next = NULL;
}

node_t *sorted_merge(node_t **head_a, node_t **head_b, int (*cmp)(const void *, const void *))
{
    if (!head_a || !head_b || !cmp)
        return NULL;

    node_t dummy;
    node_t *current = &dummy;
    dummy.next = NULL;

    while (*head_a && *head_b)
    {
        if (cmp((*head_a)->data, (*head_b)->data) <= 0)
        {
            current->next = *head_a;
            *head_a = (*head_a)->next;
        }
        else
        {
            current->next = *head_b;
            *head_b = (*head_b)->next;
        }
        current = current->next;
    }

    // Один из списков может быть длиннее
    current->next = *head_a ? *head_a : *head_b;

    *head_a = NULL;
    *head_b = NULL;

    return dummy.next;
}

// endregion

// region MY

node_t *list_new(void *data)
{
    node_t *new_node = (node_t *)malloc(sizeof(node_t));
    if (new_node == NULL)
    {
        perror("Failed to allocate memory for new node");
        exit(EXIT_FAILURE);
    }
    new_node->data = data;
    new_node->next = NULL;
    return new_node;
}

void list_free(node_t *head)
{
    while (head)
    {
        node_t *tmp = head;
        head = head->next;
        free(tmp);
    }
}

int list_is_equal(const node_t *list_a, const node_t *list_b, compare_func_t cmp)
{
    while (list_a != NULL && list_b != NULL)
    {
        if (cmp(list_a->data, list_b->data) != 0)
        {
            // Если элементы не равны, списки не равны
            return 0;
        }

        list_a = list_a->next;
        list_b = list_b->next;
    }

    // Если один из списков не пуст, они не равны
    if (list_a != NULL || list_b != NULL)
    {
        return 0;
    }
    // Списки равны
    return 1;
}
