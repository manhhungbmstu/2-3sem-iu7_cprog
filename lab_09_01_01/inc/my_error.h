#ifndef _MY_ERROR_H__
#define _MY_ERROR_H__

#include <errno.h>

#define OK 0

#define ERR_STR_RANGE -1
#define ERR_INT_RANGE -2
#define ERR_ARRAY_RANGE -3
#define ERR_WRITE -4
#define ERR_STR_IO -5
#define ERR_INT_IO -6
#define ERR_IO -7

#define ERR_MEM -8

#define ERR_USAGE -10

void err_print(const int err_code);

#endif
