#ifndef MY_FUNCTION_H__
#define MY_FUNCTION_H__

#include <stdio.h>

/**
 * @brief Читать целое число с проверкой диапазона
 */
int int_read(int *data, FILE *f, const int value_min, const int value_max);


/**
 * @brief Читать строку из файла
 */
int str_read(char **data, FILE *f);
#endif
