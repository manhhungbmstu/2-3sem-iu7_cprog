#ifndef MOVIE_H__
#define MOVIE_H__

#include <stdio.h>

#define MIN_YEAR 1
#define MAX_YEAR 2099

typedef struct
{
    char *title;
    char *name;
    int year;
} movie_t;

/**
 * @brief Освобождение памяти структуры movie_t
 */
void movie_free(movie_t *ptr);

/**
 * @brief Глубокое копирование структуру
 */
int copy_movie(movie_t *dest, const movie_t *src);

/**
 * @brief Чтение структуру
 */
int movie_read(movie_t *ptr, FILE *f);
/**
 * @brief Вывод структуру
 */
int movie_write(const movie_t *ptr, FILE *f);

/**
 * @brief Сранение по 'название кинофильма'
 */
int movie_cmp_by_title(const movie_t *pl, const movie_t *pr);
/**
 * @brief Сранение по 'фамилия режиссёра'
 */
int movie_cmp_by_name(const movie_t *pl, const movie_t *pr);
/**
 * @brief Сранение по 'год выхода'
 */
int movie_cmp_by_year(const movie_t *pl, const movie_t *pr);

#endif
