#ifndef ARRAY_H__
#define ARRAY_H__

#include <stdio.h>
#include "movie.h"

typedef int (*cmp_func_t)(const movie_t *, const movie_t *);
/**
 * @brief Освобождение памяти элементов массива
 *
 */
void free_array(movie_t *array, const size_t n);

/**
 * @brief Считать количество элементов в файле
 * @return int Код ошибки
 */
int array_count(size_t *n, FILE *f);

/**
 * @brief Читать в массив в отсортированном месте

 */
int array_read_sorted(movie_t *array, const size_t n_max, size_t *n, FILE *f, cmp_func_t cmp_func);

/**
 * @brief Вывод элемента в файл

 */
int write_array(const movie_t *array, const size_t n_max, const size_t n, FILE *f);

/**
 * @brief Бинарный поиск элемента в массиве
 */
int array_search_binary(const movie_t *target, const movie_t *array, const size_t n_max, const size_t n, cmp_func_t cmp_func);

#endif
