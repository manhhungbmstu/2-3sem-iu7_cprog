#include <stdio.h>
#include <stdlib.h>

#include "check_array.h"
#include "my_error.h"
#include "array.h"
#include "movie.h"

// Чтение, сортировка по `title`
START_TEST(test_array_read_sorted_by_title)
{
    FILE *file = tmpfile();
    ck_assert_ptr_nonnull(file);

    movie_t movies[] = {
        {.title = "Inception", .name = "Christopher Nolan", .year = 2010},
        {.title = "The Shawshank Redemption", .name = "Frank Darabont", .year = 1994},
        {.title = "The Godfather", .name = "Francis Ford Coppola", .year = 1972},
        {.title = "Pulp Fiction", .name = "Quentin Tarantino", .year = 1994},
        {.title = "The Dark Knight", .name = "Christopher Nolan", .year = 2008},
        {.title = "Forrest Gump", .name = "Robert Zemeckis", .year = 1994},
        {.title = "The Matrix", .name = "Lana Wachowski, Lilly Wachowski", .year = 1999}};

    for (size_t i = 0; i < sizeof(movies) / sizeof(movies[0]); i++)
    {
        if (movie_write(&movies[i], file) != OK)
        {
            perror("Error writing to file");
            exit(EXIT_FAILURE);
        }
    }

    rewind(file);

    const size_t n_max = sizeof(movies) / sizeof(movies[0]);
    movie_t *array = malloc(n_max * sizeof(movie_t));
    size_t n = 0;

    int rc = array_read_sorted(array, n_max, &n, file, movie_cmp_by_title);

    ck_assert_int_eq(OK, rc);

    for (size_t i = 0; i < n - 1; i++)
    {
        ck_assert(movie_cmp_by_title(array + i, array + i + 1) <= 0);
    }

    for (size_t i = 0; i < n; i++)
    {
        movie_free(array + i);
    }

    free(array);
    fclose(file);
}
END_TEST

// Чтение, сортировка по `name`
START_TEST(test_array_read_sorted_by_name)
{
    FILE *file = tmpfile();
    ck_assert_ptr_nonnull(file);

    movie_t movies[] = {
        {.title = "Inception", .name = "Christopher Nolan", .year = 2010},
        {.title = "The Shawshank Redemption", .name = "Frank Darabont", .year = 1994},
        {.title = "The Godfather", .name = "Francis Ford Coppola", .year = 1972},
        {.title = "Pulp Fiction", .name = "Quentin Tarantino", .year = 1994},
        {.title = "The Dark Knight", .name = "Christopher Nolan", .year = 2008},
        {.title = "Forrest Gump", .name = "Robert Zemeckis", .year = 1994},
        {.title = "The Matrix", .name = "Lana Wachowski, Lilly Wachowski", .year = 1999}};


    for (size_t i = 0; i < sizeof(movies) / sizeof(movies[0]); i++)
    {
        if (movie_write(&movies[i], file) != OK)
        {
            perror("Error writing to file");
            exit(EXIT_FAILURE);
        }
    }

    rewind(file);

    const size_t n_max = sizeof(movies) / sizeof(movies[0]);
    movie_t *array = malloc(n_max * sizeof(movie_t));
    size_t n = 0;

    int rc = array_read_sorted(array, n_max, &n, file, movie_cmp_by_name);

    // Check the return code
    ck_assert_int_eq(OK, rc);

    for (size_t i = 0; i < n - 1; i++)
    {
        ck_assert(movie_cmp_by_name(array + i, array + i + 1) <= 0);
    }

    for (size_t i = 0; i < n; i++)
    {
        movie_free(array + i);
    }

    free(array);
    fclose(file);
}
END_TEST

// Чтение, сортировка по `year`
START_TEST(test_array_read_sorted_by_year)
{
    FILE *file = tmpfile();
    ck_assert_ptr_nonnull(file);

    movie_t movies[] = {
        {.title = "Inception", .name = "Christopher Nolan", .year = 2010},
        {.title = "The Shawshank Redemption", .name = "Frank Darabont", .year = 1994},
        {.title = "The Godfather", .name = "Francis Ford Coppola", .year = 1972},
        {.title = "Pulp Fiction", .name = "Quentin Tarantino", .year = 1994},
        {.title = "The Dark Knight", .name = "Christopher Nolan", .year = 2008},
        {.title = "Forrest Gump", .name = "Robert Zemeckis", .year = 1994},
        {.title = "The Matrix", .name = "Lana Wachowski, Lilly Wachowski", .year = 1999}};


    for (size_t i = 0; i < sizeof(movies) / sizeof(movies[0]); i++)
    {
        if (movie_write(&movies[i], file) != OK)
        {
            perror("Error writing to file");
            exit(EXIT_FAILURE);
        }
    }

    rewind(file);

    const size_t n_max = sizeof(movies) / sizeof(movies[0]);
    movie_t *array = malloc(n_max * sizeof(movie_t));
    size_t n = 0;

    int rc = array_read_sorted(array, n_max, &n, file, movie_cmp_by_year);

    ck_assert_int_eq(OK, rc);

    for (size_t i = 0; i < n - 1; i++)
    {
        ck_assert(movie_cmp_by_year(array + i, array + i + 1) <= 0);
    }

    for (size_t i = 0; i < n; i++)
    {
        movie_free(array + i);
    }

    free(array);
    fclose(file);
}
END_TEST

// Искать по `title`
START_TEST(test_array_search_binary_by_title)
{
    movie_t movies[] = {
        {.title = "Inception", .name = "Christopher Nolan", .year = 2010},
        {.title = "The Shawshank Redemption", .name = "Frank Darabont", .year = 1994},
        {.title = "The Godfather", .name = "Francis Ford Coppola", .year = 1972},
        {.title = "Pulp Fiction", .name = "Quentin Tarantino", .year = 1994},
        {.title = "The Dark Knight", .name = "Christopher Nolan", .year = 2008},
        {.title = "Forrest Gump", .name = "Robert Zemeckis", .year = 1994},
        {.title = "The Matrix", .name = "Lana Wachowski, Lilly Wachowski", .year = 1999}};

    const size_t n = sizeof(movies) / sizeof(movies[0]);

    movie_t target = {.title = "Pulp Fiction", .name = "", .year = 0};
    size_t result_index = array_search_binary(&target, movies, n, n, movie_cmp_by_title);

    ck_assert_int_eq(result_index, 3);

    movie_t non_existent_target = {.title = "Nonexistent-Title", .name = "", .year = 0};
    size_t non_existent_result_index = array_search_binary(&non_existent_target, movies, n, n, movie_cmp_by_title);

    ck_assert_int_eq(non_existent_result_index, -1);
}
END_TEST


// Искать по `name`
START_TEST(test_array_search_binary_by_name)
{
    movie_t movies[] = {
        {.title = "Inception", .name = "Christopher Nolan", .year = 2010},
        {.title = "The Shawshank Redemption", .name = "Frank Darabont", .year = 1994},
        {.title = "The Godfather", .name = "Francis Ford Coppola", .year = 1972},
        {.title = "Pulp Fiction", .name = "Quentin Tarantino", .year = 1994},
        {.title = "The Dark Knight", .name = "Christopher Nolan", .year = 2008},
        {.title = "Forrest Gump", .name = "Robert Zemeckis", .year = 1994},
        {.title = "The Matrix", .name = "Lana Wachowski, Lilly Wachowski", .year = 1999}};

    const size_t n = sizeof(movies) / sizeof(movies[0]);

    // Search for a movie by name
    movie_t target = {.title = "", .name = "Christopher Nolan", .year = 0};
    size_t result_index = array_search_binary(&target, movies, n, n, movie_cmp_by_name);

    ck_assert_int_eq(result_index, 0);

    movie_t non_existent_target = {.title = "", .name = "Nonexistent-Name", .year = 0};
    size_t non_existent_result_index = array_search_binary(&non_existent_target, movies, n, n, movie_cmp_by_name);

    ck_assert_int_eq(non_existent_result_index, -1);
}
END_TEST


// Считать количество
START_TEST(test_array_count)
{
    FILE *file = tmpfile();
    ck_assert_ptr_nonnull(file);

    movie_t movies[] = {
        {.title = "Inception", .name = "Christopher Nolan", .year = 2010},
        {.title = "The Shawshank Redemption", .name = "Frank Darabont", .year = 1994},
        {.title = "The Godfather", .name = "Francis Ford Coppola", .year = 1972},
        {.title = "Pulp Fiction", .name = "Quentin Tarantino", .year = 1994},
        {.title = "The Dark Knight", .name = "Christopher Nolan", .year = 2008},
        {.title = "Forrest Gump", .name = "Robert Zemeckis", .year = 1994},
        {.title = "The Matrix", .name = "Lana Wachowski, Lilly Wachowski", .year = 1999}};

    for (size_t i = 0; i < sizeof(movies) / sizeof(movies[0]); i++)
    {
        if (movie_write(&movies[i], file) != OK)
        {
            perror("Error writing to file");
            exit(EXIT_FAILURE);
        }
    }

    rewind(file);

    size_t count = 0;
    int rc = array_count(&count, file);

    ck_assert_int_eq(OK, rc);

    ck_assert_int_eq(count, sizeof(movies) / sizeof(movies[0]));

    fclose(file);
}
END_TEST

START_TEST(test_array_read_sorted)
{
    FILE *file = tmpfile();
    ck_assert_ptr_nonnull(file);

    movie_t movies[] = {
        {.title = "Inception", .name = "Christopher Nolan", .year = 2010},
        {.title = "The Shawshank Redemption", .name = "Frank Darabont", .year = 1994},
        {.title = "The Godfather", .name = "Francis Ford Coppola", .year = 1972},
        {.title = "Pulp Fiction", .name = "Quentin Tarantino", .year = 1994},
        {.title = "The Dark Knight", .name = "Christopher Nolan", .year = 2008},
        {.title = "Forrest Gump", .name = "Robert Zemeckis", .year = 1994},
        {.title = "The Matrix", .name = "Lana Wachowski, Lilly Wachowski", .year = 1999}};

    for (size_t i = 0; i < sizeof(movies) / sizeof(movies[0]); i++)
    {
        if (movie_write(&movies[i], file) != OK)
        {
            perror("Error writing to file");
            exit(EXIT_FAILURE);
        }
    }

    rewind(file);

    const size_t n_max = sizeof(movies) / sizeof(movies[0]);
    movie_t *array = malloc(n_max * sizeof(movie_t));
    ;
    size_t n = 0;

    int rc = array_read_sorted(array, n_max, &n, file, movie_cmp_by_title);

    ck_assert_int_eq(OK, rc);

    for (size_t i = 0; i < n - 1; i++)
    {
        ck_assert(movie_cmp_by_title(array + i, array + i + 1) <= 0);
    }

    for (size_t i = 0; i < n; i++)
    {
        movie_free(array + i);
    }

    free(array);
    fclose(file);
}
END_TEST

Suite *array_suite(void)
{
    Suite *suite = suite_create("array");

    TCase *tc_pos = tcase_create("positives");
    tcase_add_test(tc_pos, test_array_read_sorted);
    tcase_add_test(tc_pos, test_array_read_sorted_by_title);
    tcase_add_test(tc_pos, test_array_read_sorted_by_name);
    tcase_add_test(tc_pos, test_array_read_sorted_by_year);

    tcase_add_test(tc_pos, test_array_search_binary_by_title);
    tcase_add_test(tc_pos, test_array_search_binary_by_name);

    tcase_add_test(tc_pos, test_array_count);

    suite_add_tcase(suite, tc_pos);
    return suite;
}
