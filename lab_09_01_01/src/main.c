#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "my_error.h"
#include "array.h"
#include "movie.h"

int processfile(const char *filename, const char *attribute, const char *searchvalue);

int main(int argc, char **argv)
{
    if (argc != 3 && argc != 4)
    {
        // fprintf(stderr, "Usage: %s <filename> <attribute> [<searchvalue>]\n", argv[0]);
        return ERR_USAGE;
    }

    int rc = processfile(argv[1], argv[2], (argc == 4) ? argv[3] : NULL);

    if (rc)
        err_print(rc);

    return rc;
}

int processfile(const char *filename, const char *attribute, const char *searchvalue)
{
    FILE *file = fopen(filename, "r");
    if (!file)
    {
        int rc = errno;
        err_print(rc);
        return rc;
    }

    movie_t *movies = NULL;
    size_t n;

    cmp_func_t cmp = NULL;

    if (strcmp(attribute, "title") == 0)
        cmp = movie_cmp_by_title;
    else if (strcmp(attribute, "name") == 0)
        cmp = movie_cmp_by_name;
    else if (strcmp(attribute, "year") == 0)
        cmp = movie_cmp_by_year;
    if (!cmp)
    {
        fclose(file);
        return ERR_USAGE;
    }
    size_t n_max = 0;
    int rc = array_count(&n_max, file);
    if (rc || n_max == 0)
    {
        fclose(file);
        return (rc) ? rc : ERR_ARRAY_RANGE;
    }
    rewind(file);
    movies = calloc(n_max, sizeof(movie_t));
    if (!movies)
    {
        fclose(file);
        return ERR_MEM;
    }
    rc = array_read_sorted(movies, n_max, &n, file, cmp);
    if (rc)
    {
        fclose(file);

        for (size_t i = 0; i < n_max; i++)
        {
            movie_free(&movies[i]);
        }

        free(movies);

        return rc;
    }
    if (searchvalue)
    {
        movie_t target = { 0 };

        if (strcmp(attribute, "title") == 0)
            target.title = strdup(searchvalue);
        else if (strcmp(attribute, "name") == 0)
            target.name = strdup(searchvalue);
        else if (strcmp(attribute, "year") == 0)
        {
            char *endptr;
            int year = strtol(searchvalue, &endptr, 10);
            if (*endptr == 0 && year >= MIN_YEAR && year <= MAX_YEAR)
                target.year = year;
            else
                rc = ERR_USAGE;
        }

        if (!rc)
        {
            int idx = array_search_binary(&target, movies, n_max, n, cmp);
            if (idx == -1)
                printf("Not found\n");
            else
            {
                rc = movie_write(&movies[idx], stdout);
                if (rc != OK)
                {
                    fclose(file);

                    for (size_t i = 0; i < n_max; i++)
                    {
                        movie_free(&movies[i]);
                    }

                    free(movies);

                    free(target.title);
                    free(target.name);

                    return rc;
                }
            }
        }

        free(target.title);
        free(target.name);
    }
    else
    {
        rc = write_array(movies, n_max, n, stdout);
    }

    fclose(file);

    for (size_t i = 0; i < n_max; i++)
    {
        movie_free(&movies[i]);
    }

    free(movies);

    return rc;
}
