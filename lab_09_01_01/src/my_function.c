#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "my_error.h"
#include "my_function.h"
#include <assert.h>
#include "my_error.h"


int int_read(int *data, FILE *f, const int min, const int max)
{
    assert(min <= max);

    int number;

    if (fscanf(f, "%d", &number) != 1)
    {
        return ERR_INT_IO;
    }

    if (number < min || number > max)
    {
        return ERR_INT_RANGE;
    }

    *data = number;
    return OK;
}
int str_read(char **data, FILE *f)
{
    char *line = NULL;
    size_t len = 0;
    ssize_t nread;

    nread = getline(&line, &len, f);

    if (nread == -1)
    {
        free(line);
        return ERR_STR_IO;
    }

    if (nread > 0 && line[nread - 1] == '\n')
    {
        line[--nread] = '\0';
    }

    if (nread == 0)
    {
        free(line);
        return ERR_STR_RANGE;
    }

    *data = line;
    return OK;
}
