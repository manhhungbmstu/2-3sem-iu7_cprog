#define _POSIX_C_SOURCE 200809L

#include <stdlib.h>
#include <string.h>

#include "movie.h"
#include "my_error.h"
#include "my_function.h"
#include <stdbool.h>
#include <stdlib.h>

void movie_free(movie_t *ptr)
{
    if (ptr != NULL)
    {
        free(ptr->title);
        free(ptr->name);
        ptr->title = ptr->name = NULL;
    }
}
bool duplicate_string(char **dest, const char *src)
{
    if (src != NULL)
    {
        *dest = strdup(src);
        return (*dest != NULL);
    }
    else
    {
        *dest = NULL;
        return true;
    }
}

int copy_movie(movie_t *dest, const movie_t *src)
{
    bool duplicate_string(char **dest, const char *src)
    {
        *dest = strdup(src);
        return (*dest != NULL);
    }

    if (!duplicate_string(&dest->title, src->title))
        return ERR_MEM;


    if (!duplicate_string(&dest->name, src->name))
    {
        free(dest->title);
        dest->title = NULL;
        return ERR_MEM;
    }

    dest->year = src->year;

    return OK;
}
int movie_read(movie_t *ptr, FILE *f)
{
    int rc = OK;

    rc = str_read(&(ptr->title), f);
    if (rc == OK)
    {
        rc = str_read(&(ptr->name), f);
        if (rc == OK)
        {
            rc = int_read(&(ptr->year), f, MIN_YEAR, MAX_YEAR);
            if (rc != OK)
            {
                free(ptr->name);
                ptr->name = NULL;
            }
        }

        if (rc != OK)
        {
            free(ptr->title);
            ptr->title = NULL;
        }
    }

    char temp[2];
    fgets(temp, sizeof(temp), f);

    return rc;
}

int movie_write(const movie_t *ptr, FILE *f)
{
    int rc = OK;

    if (fprintf(f, "%s\n", ptr->title) < 0 ||
        fprintf(f, "%s\n", ptr->name) < 0 ||
        fprintf(f, "%d\n", ptr->year) < 0)
    {
        rc = ERR_WRITE;
    }

    return rc;
}

int movie_cmp_by_title(const movie_t *pl, const movie_t *pr)
{
    const char *title1 = pl->title;
    const char *title2 = pr->title;

    while (*title1 != '\0' && *title2 != '\0')
    {
        if (*title1 < *title2)
        {
            return -1;
        }
        else if (*title1 > *title2)
        {
            return 1;
        }
        title1++;
        title2++;
    }

    if (*title1 == '\0' && *title2 == '\0')
    {
        return 0;
    }
    else if (*title1 == '\0')
    {
        // Title 1 is shorter, so it comes before Title 2
        return -1;
    }
    else
    {
        // Title 2 is shorter, so it comes before Title 1
        return 1;
    }
}
int movie_cmp_by_name(const movie_t *pl, const movie_t *pr)
{
    const char *name1 = pl->name;
    const char *name2 = pr->name;

    while (*name1 != '\0' && *name2 != '\0')
    {
        if (*name1 < *name2)
        {
            return -1;
        }
        else if (*name1 > *name2)
        {
            return 1;
        }
        name1++;
        name2++;
    }

    if (*name1 == '\0' && *name2 == '\0')
    {
        return 0;
    }
    else if (*name1 == '\0')
    {
        // Title 1 is shorter, so it comes before Title 2
        return -1;
    }
    else
    {
        // Title 2 is shorter, so it comes before Title 1
        return 1;
    }
}
int movie_cmp_by_year(const movie_t *pl, const movie_t *pr)
{
    if (pl->year < pr->year)
    {
        return -1;
    }
    else if (pl->year > pr->year)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
