#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "array.h"
#include "my_error.h"

void free_array(movie_t *array, const size_t n)
{
    for (size_t i = 0; i < n; i++)
    {
        movie_free(&array[i]);
    }
}

int array_count(size_t *n, FILE *f)
{
    int rc = OK;
    *n = 0;

    while (1)
    {
        movie_t *tmp = calloc(1, sizeof(movie_t));
        if (!tmp)
        {
            rc = ERR_MEM;
            break;
        }

        rc = movie_read(tmp, f);
        if (rc != OK)
        {
            movie_free(tmp);
            free(tmp);
            break;
        }

        movie_free(tmp);
        (*n)++;
        free(tmp);
    }
    if (feof(f))
        rc = OK;

    return rc;
}

int array_read_sorted(movie_t *array, const size_t n_max, size_t *n, FILE *f, cmp_func_t cmp_func)
{
    int rc = OK;
    *n = 0;

    while (*n < n_max)
    {
        if (*n > n_max)
            return ERR_MEM;
        movie_t tmp;
        rc = movie_read(&tmp, f);

        if (rc == OK)
        {
            size_t i = 0;
            while (i < *n && cmp_func(array + i, &tmp) <= 0)
            {
                i++;
            }

            for (size_t j = *n; j > i; j--)
            {
                if (j < n_max)
                {
                    if (j > 0)
                    {
                        copy_movie(array + j, array + j - 1);
                        movie_free(array + j - 1);
                    }
                }
            }

            if (i < n_max)
            {
                copy_movie(array + i, &tmp);
                (*n)++;
            }
            movie_free(&tmp);
        }
        else if (rc == ERR_STR_RANGE || rc == ERR_STR_IO)
        {
            rc = OK;
            movie_free(&tmp);
        }
        else
        {
            movie_free(&tmp);
            break;
        }
    }

    return rc;
}

int write_array(const movie_t *array, const size_t n_max, const size_t n, FILE *f)
{
    if (n > n_max)
        return ERR_MEM;

    for (size_t i = 0; i < n; i++)
    {
        int rc = movie_write(array + i, f);
        if (rc != OK)
        {
            return rc;
        }
    }

    return OK;
}

int array_search_binary(const movie_t *target, const movie_t *array, size_t n_max, size_t n, cmp_func_t cmp_func)
{
    if (n > n_max)
        return ERR_MEM;

    size_t begin = 0;
    size_t end = n;

    while (begin < end)
    {
        size_t mid = begin + (end - begin) / 2;
        const movie_t *mid_elem = &array[mid];

        int cmp_result = cmp_func(mid_elem, target);

        if (cmp_result == 0)
        {
            return mid;
        }

        if (cmp_result < 0)
        {
            begin = mid + 1;
        }
        else
        {
            end = mid;
        }
    }

    return -1;
}