#include <stdio.h>
#include <string.h>

#include "my_error.h"

#ifdef MY_DEBUG

#else

void err_print(const int err_code)
{
    (void)err_code;
}

#endif
