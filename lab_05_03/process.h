#ifndef _PROCESS_H_
#define _PROCESS_H_

#include <stdio.h>
/**
 * @brief Записать n случайные числа в файл
 */
int process_c(FILE *f, char *number);

/**
 * @brief Печатать содержимое файла на экран

 */
int process_p(FILE *f);

/**
 * @brief Сортировать числа в файле
 */
int process_s(FILE *f);

/**
 * @brief Конвертировать текстовый файл со структурой student_t в бинарный
 */
int process_t2b(char *file_txt, char *file_bin);

/**
 * @brief Конвертировать бинарный файл со структурой student_t в текстовый
 */
int process_b2t(char *file_bin, char *file_txt);

#endif
