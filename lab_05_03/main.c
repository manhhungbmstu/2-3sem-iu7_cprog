#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "process.h"
#include "file.h"
void usage(void)
{
    // создавать файл и заполнять его случайными числами
    printf("app.exe c количество имя файл\n");
    // выводить числа из файла на экран
    printf("app.exe p имя файл\n");
    // упорядочивать числа в файле
    printf("app.exe s file\n");

    // Перевод текстовый файл в бинарный
    printf("app.exe t2b файл_txt файл_bin\n");

    // Перевод бинарный файл в текстовый
    printf("app.exe b2t файл_bin файл_txt\n");
}

int main(int argc, char **argv)
{
    int rc = OK;
    FILE *f;

    if (argc < 2)
    {
        usage();
        return ERR_USAGE;
    }

    if (strcmp(argv[1], "c") == 0)
    {
        if (argc != 4)
        {
            usage();
            return ERR_USAGE;
        }

        f = fopen(argv[3], "wb");
        if (f == NULL)
        {
            rc = errno;
            perror("Error opening file for writing");
            return rc;
        }

        rc = process_c(f, argv[2]);

        fclose(f);
        return rc;
    }
    else if (strcmp(argv[1], "p") == 0)
    {
        if (argc != 3)
        {
            usage();
            return ERR_USAGE;
        }

        f = fopen(argv[2], "rb");
        if (f == NULL)
        {
            rc = errno;
            perror("Error opening file for reading");
            return rc;
        }

        rc = process_p(f);

        fclose(f);
        return rc;
    }
    else if (strcmp(argv[1], "s") == 0)
    {
        if (argc != 3)
        {
            usage();
            return ERR_USAGE;
        }

        f = fopen(argv[2], "rb+");
        if (f == NULL)
        {
            rc = errno;
            perror("Error opening file for read/write");
            return rc;
        }

        rc = process_s(f);

        fclose(f);
        return rc;
    }
    else if (strcmp(argv[1], "t2b") == 0)
    {
        if (argc != 4)
        {
            usage();
            return ERR_USAGE;
        }

        rc = process_t2b(argv[2], argv[3]);
        return rc;
    }
    else if (strcmp(argv[1], "b2t") == 0)
    {
        if (argc != 4)
        {
            usage();
            return ERR_USAGE;
        }

        rc = process_b2t(argv[2], argv[3]);
        return rc;
    }
    else
    {
        usage();
        return ERR_USAGE;
    }
}
