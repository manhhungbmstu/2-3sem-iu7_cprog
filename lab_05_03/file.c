#include <assert.h>
#include <stdlib.h>
#include "file.h"

int write_file(FILE *f, char *number)
{
    assert(f != NULL);
    assert(number != NULL);

    char *end;
    long n = strtol(number, &end, 0);

    if (*end != '\0' || n <= 0)
    {
        return ERR_RANGE;
    }

    int random;
    for (long i = 0; i < n; i++)
    {
        random = rand() % 100;

        if (fwrite(&random, sizeof(int), 1, f) != 1)
        {
            return ERR_WRITE;
        }
    }

    return OK;
}

int get_file_size(FILE *f, size_t *size)
{
    assert(f != NULL);
    assert(size != NULL);

    if (fseek(f, 0L, SEEK_END) != 0)
    {
        return 1;
    }

    long sz = ftell(f);
    if (sz < 0)
    {
        return 1;
    }

    *size = (size_t)sz;

    if (fseek(f, 0L, SEEK_SET) != 0)
    {
        return 1;
    }

    return 0;
}

int output_file(FILE *f, FILE *f_out)
{
    assert(f != NULL);

    size_t size;
    int num;

    if (get_file_size(f, &size))
        return ERR_GET_FILE_SIZE;

    size_t total = size / sizeof(int);
    if (total == 0)
        return ERR_EMPTY;
    if (size % sizeof(int))
        return ERR_FILE;

    for (size_t i = 0; i < total - 1; i++)
    {
        if (fread(&num, sizeof(int), 1, f) != 1)
            return ERR_READ;

        fprintf(f_out, "%d ", num);
    }
    if (fread(&num, sizeof(int), 1, f) != 1)
        return ERR_READ;
    fprintf(f_out, "%d\n", num);

    return OK;
}

/**
По заданной позиции прочитать число в указанной позиции
 */
int get_number_by_pos(FILE *f, int pos, int *num)
{
    assert(f != NULL);
    assert(num != NULL);

    if (fseek(f, pos * sizeof(int), SEEK_SET) != 0)
    {
        return ERR_SEEK;
    }

    size_t elements_read = fread(num, sizeof(int), 1, f);
    if (elements_read != 1)
    {
        if (feof(f))
        {
            return ERR_READ;
        }
        else if (ferror(f))
        {
            return ERR_READ;
        }
    }

    return OK;
}

/**
Записать число на указанную позицию с затиранием
 */
int put_number_by_pos(FILE *f, int pos, int num)
{
    assert(f != NULL);

    if (fseek(f, pos * sizeof(int), SEEK_SET) != 0)
    {
        return ERR_SEEK;
    }

    size_t elements_written = fwrite(&num, sizeof(int), 1, f);
    if (elements_written != 1)
    {
        return ERR_WRITE;
    }

    return OK;
}

int sort_file(FILE *f)
{
    assert(f != NULL);

    int rc = OK;
    size_t size;
    int num1, num2;
    int swapped;

    if (get_file_size(f, &size) != OK)
    {
        return ERR_GET_FILE_SIZE;
    }

    int total = size / sizeof(int);
    if (total == 0)
    {
        return ERR_EMPTY;
    }
    if (size % sizeof(int) != 0)
    {
        return ERR_FILE;
    }

    for (int i = 0; i < total - 1; i++)
    {
        swapped = 0;

        for (int j = 0; j < total - i - 1; j++)
        {
            rc = get_number_by_pos(f, j, &num1);
            if (rc != OK)
            {
                return rc;
            }
            rc = get_number_by_pos(f, j + 1, &num2);
            if (rc != OK)
            {
                return rc;
            }
            if (num1 > num2)
            {
                rc = put_number_by_pos(f, j, num2);
                if (rc != OK)
                {
                    return rc;
                }
                rc = put_number_by_pos(f, j + 1, num1);
                if (rc != OK)
                {
                    return rc;
                }
                swapped = 1;
            }
        }

        if (swapped == 0)
        {
            break;
        }
    }

    return OK;
}
