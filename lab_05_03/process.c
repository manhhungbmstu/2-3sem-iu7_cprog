#include <assert.h>
#include "process.h"
#include "file.h"

int process_c(FILE *f, char *number)
{
    if (f == NULL || number == NULL)
    {
        return -1;
    }

    return write_file(f, number);
}

int process_p(FILE *f)
{
    if (f == NULL)
    {
        return -1;
    }

    return output_file(f, stdout);
}

int process_s(FILE *f)
{
    if (f == NULL)
    {
        return -1;
    }

    return sort_file(f);
}

int process_t2b(char *file_txt, char *file_bin)
{
    int rc = OK;
    FILE *f_src = fopen(file_txt, "r");
    FILE *f_dst = fopen(file_bin, "wb");

    if (f_src == NULL || f_dst == NULL)
    {
        rc = errno;
        if (f_src != NULL)
            fclose(f_src);
        if (f_dst != NULL)
            fclose(f_dst);
        return rc;
    }

    int num;
    while (fscanf(f_src, "%d", &num) == 1)
    {
        if (fwrite(&num, sizeof(num), 1, f_dst) != 1)
        {
            rc = errno;
            break;
        }
    }

    fclose(f_src);
    fclose(f_dst);
    return rc;
}

int process_b2t(char *file_bin, char *file_txt)
{
    int rc = OK;
    FILE *f_src, *f_dst;

    f_src = fopen(file_bin, "rb");
    f_dst = fopen(file_txt, "w");
    if (f_src == NULL || f_dst == NULL)
    {
        rc = errno;
        if (f_src != NULL)
            fclose(f_src);
        if (f_dst != NULL)
            fclose(f_dst);
        return rc;
    }

    rc = output_file(f_src, f_dst);

    fclose(f_src);
    fclose(f_dst);
    return rc;
}
