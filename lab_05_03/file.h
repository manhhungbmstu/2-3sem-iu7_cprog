#ifndef _FILE_H_
#define _FILE_H_
#include <errno.h>
#include <stdio.h>
#define OK 0                 /** Все хорошо */
#define ERR_READ -1          /** ошибка: Чтение файла */
#define ERR_WRITE -2         /** ошибка: Запись в файл */
#define ERR_SEEK -4          /** ошибка: Установка позиции в потоке данных */
#define ERR_GET_FILE_SIZE -3 /** ошибка: Определение размер файла */
#define ERR_USAGE 53         /** ошибка: Использование */
#define ERR_RANGE -16        /** ошибка: Значение вне допустимого диапазона */
#define ERR_EMPTY -17        /** ошибка: Нет цифры в файле */
#define ERR_FILE -18         /** ошибка: Файл поврежден */
/**
 * @brief Записать n случайные числа в файл
 */
int write_file(FILE *f, char *number);

/**
 * @brief Печатать содержимое файла в другой файл
 */
int output_file(FILE *f, FILE *f_out);

/**
 * @brief Сортировать числа в файле
 */
int sort_file(FILE *f);

/**
 * @brief Подсчитать размер файла
 */
int get_file_size(FILE *f, size_t *size);

#endif // _FILE_H_
