#ifndef _FUNC_MATRIX_H_
#define _FUNC_MATRIX_H_

#include <math.h>

#include "error.h"
#include "func_IO.h"
int search_row(matrix a);
void free_row(matrix *a, int numb_row);
void del_rows(matrix *a);
void del_row_with_min_element(matrix *a);

#endif
