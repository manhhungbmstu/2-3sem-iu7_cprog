#include "../inc/func_matrix.h"

int search_row(matrix a)
{
    int i_min = 0, j_min = 0;

    for (int j = 0; j < a.c; j++)
        for (int i = 0; i < a.r; i++)
            if (a.matrix[i][j] <= a.matrix[i_min][j_min])
            {
                i_min = i;
                j_min = j;
            }

    return i_min;
}

void free_row(matrix *a, int numb_row)
{
    free(a->matrix[numb_row]);

    for (int i = numb_row; i < a->r - 1; i++)
        a->matrix[i] = a->matrix[i + 1];
}

void del_rows(matrix *a)
{
    int numb_row;

    numb_row = search_row(*a);
    free_row(a, numb_row);
    (a->r)--;
}
void del_row_with_min_element(matrix *a)
{
    int i_min = 0, j_min = 0;

    for (int j = 0; j < a->c; j++)
    {
        for (int i = 0; i < a->r; i++)
        {
            if (a->matrix[i][j] < a->matrix[i_min][j_min])
            {
                i_min = i;
                j_min = j;
            }
        }
    }

    free(a->matrix[i_min]);

    for (int i = i_min; i < a->r - 1; i++)
    {
        a->matrix[i] = a->matrix[i + 1];
    }

    (a->r)--;
}
