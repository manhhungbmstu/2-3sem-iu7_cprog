#include "../inc/func_IO.h"
#include "../inc/func_matrix.h"
#include "../inc/error.h"


void message_error(int rc)
{
    switch (rc)
    {
        case ERR_INPUT:
            puts("error input\n");
            break;
        case ERR_MEMORY:
            puts("error memory\n");
        default:
            break;
    }
}
int main(void)
{   
    matrix a;

    int rc = read_matrices(&a);

    if (rc)
    {
        message_error(rc);
        return rc;
    }

    del_row_with_min_element(&a); 

    print_matrix(&a);

    free_mem(a.matrix, a.r);
    return rc;
}

