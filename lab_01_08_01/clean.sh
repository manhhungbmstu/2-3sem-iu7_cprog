#!/bin/bash

rm -rf "*.exe"
rm -rf "*.o"
rm -rf "*.out"
rm -rf "*.gcno"
rm -rf "*.gcda"
rm -rf "*.gcov"
