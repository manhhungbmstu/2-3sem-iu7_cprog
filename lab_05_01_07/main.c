#include "file.h"
void print_error(int err);
int main(void)
{
    int ret_code = OK;
    int count;
    FILE *f = stdin;
    ret_code = process(f, &count);
    if (ret_code)
    {
        print_error(ret_code);
        return ret_code;
    }

    printf("%d\n", count);

    return OK;
}
void print_error(int err)
{
    switch (err)
    {
        case ERR_IO:
            printf("Error: Ввод/Вывод\n");
            break;
        case OK:
            break;
        default:
            printf("Error: Неизвестный код!\n");
            break;
    }
}