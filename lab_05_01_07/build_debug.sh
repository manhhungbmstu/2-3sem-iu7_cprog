#!/bin/bash

OPTIONS="-std=c99 -Wall -Werror -Wextra -Wpedantic -Wfloat-equal -Wfloat-conversion -Wvla -g -fprofile-arcs -ftest-coverage"

if [[ $# -eq 1 ]]; then
    gcc "$1" $OPTIONS -c ./*.c
else
    gcc $OPTIONS -c ./*.c
fi

gcc $OPTIONS -o app.exe ./*.o -lm
