#ifndef _FILE_H_
#define _FILE_H_
#include <stdio.h>
#include <assert.h>
#define OK 0                 
#define ERR_IO -1 
int process(FILE *f, int *count);

#endif // _FILE_H_