#include <stdio.h>
#include <stdlib.h>
#include "array.h"

void swap(int a[N_MAX][N_MAX], size_t row, size_t col)
{
    // Индексы начала и конца области, которую нужно поменять местами
    size_t start_i = 0, end_i = row - 1;
    size_t start_j = 0, end_j = col - 1;

    while (start_i < end_i)
    {
        for (size_t j = start_j; j <= end_j; j++)
        {
            // Меняем элементы в первой и последней строке
            int temp = a[start_i][j];
            a[start_i][j] = a[end_i][j];
            a[end_i][j] = temp;
        }

        for (size_t i = start_i + 1; i < end_i; i++)
        {
            // Меняем элементы в столбцах между первой и последней строкой
            int temp = a[i][start_j];
            a[i][start_j] = a[i][end_j];
            a[i][end_j] = temp;
        }

        // Переходим к следующей внутренней области
        start_i++;
        end_i--;
        start_j++;
        end_j--;
    }
}
int main(void)
{
    size_t row = 0;
    size_t col = 0;
    int a[N_MAX][N_MAX];

    if (input_number_of_matrix(&row, &col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }
    if (row != col)
    {
        printf("Ошибка: не квадратичная матрица\n");
        return ERR;
    }
    if (input_array(a, row, col) != OK)
    {
        // printf("Error: Incorrect input\n");
        return ERR;
    }
    swap(a, row, col);

    output_array(a, row, col);

    return OK;
}
