#include <stdio.h>
#include <math.h>
#define EPS 0.0000001
#define OK 0
#define ERR 1
#define ERR_EPS -1

double f(double x);
int dbl_cmp(double dbl1, double dbl2);
double count_sum(double x, double eps);
int main(void)
{
    double x, eps;

    printf("Input x: ");
    if (scanf("%lf", &x) != 1)
    {
        return ERR;
    }

    printf("Input eps: ");
    if (scanf("%lf", &eps) != 1)
    {

        return ERR;
    }
    if (eps <= 0)
    {
        return ERR_EPS;
    }

    double sum = count_sum(x, eps);
    double y = f(x);

    printf("Sum: %0.5lf\n", sum);
    printf("f(x): %0.5lf\n", y);

    return OK;
}

int dbl_cmp(double dbl1, double dbl2)
{
    if (fabs(dbl1 - dbl2) < EPS)
        return 0;
    else if (dbl2 > dbl1)
        return 1;
    else
        return -1;
}

double count_sum(double x, double eps)
{
    double sum = 0;
    double a;
    int i = 3;

    // a_3
    a = -x / (2 * (1 - i * (i + 1)));
    while (dbl_cmp(fabs(a), eps) <= 0)
    {
        sum += a;
        i += 1;
        a *= (x * (1 - (i - 1) * i)) / ((i - 1) * (1 - i * (i + 1)));
    }

    return sum;
}

double f(double x)
{
    return sin(atan(x));
}
